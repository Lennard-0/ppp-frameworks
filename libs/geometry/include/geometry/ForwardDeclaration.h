//
// Created by lennard on 10/15/20.
//

#pragma once

#include <tt.h>
#include <sys/types.h>
#include <limits>

#ifdef __CUDA_ARCH__
  #define ENABLE_DEVICE __host__ __device__
#else
  #define ENABLE_DEVICE
#endif

namespace gb {

  using fp_t = float;
  const fp_t EPSILON = std::numeric_limits<fp_t>::epsilon();

  using PrimitiveID = int;
  const PrimitiveID INVALID_PRIMITIVE_ID = std::numeric_limits<PrimitiveID>::max();
  using RegionSizeT = PrimitiveID;
  using FacetID = u_short;
  const FacetID INVALID_FACET_ID = std::numeric_limits<FacetID>::max();

  class Triple;
  class Point;
  class Vector;
  class Ray;
  class LineSegment;
  class AABox;
  class AARectangle;
  class Plane;
  class Sphere;
  class Tetrahedron;
  class Triangle;

  struct RayIntersection;
}
