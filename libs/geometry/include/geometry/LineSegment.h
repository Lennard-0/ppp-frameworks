//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Ray.h"

namespace gb {

  class LineSegment {
  public:
    LineSegment();
    LineSegment(Point  p, Point  q);

    [[nodiscard]] const Point& p() const;
    [[nodiscard]] const Point& q() const;
    [[nodiscard]] Ray lsRay() const;

    bool parallel(const LineSegment& other) const;

  private:
    Point _p, _q;
  };
}
