//
// Created by lennard on 10/15/20.
//

#pragma once

#include "Point.h"

namespace gb {

  class Sphere {
  public:
    Sphere();
    Sphere(Point  center, const fp_t& radius);

    [[nodiscard]] const Point& center() const;
    [[nodiscard]] fp_t radius() const;

  private:
    Point _center;
    fp_t _radius;
  };
}
