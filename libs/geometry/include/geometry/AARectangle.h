//
// Created by lennard on 10/17/20.
//

#pragma once

#include "LineSegment.h"
#include <optional>

namespace gb {

  class AARectangle {
  public:
    AARectangle(u_short fixedDim, bool fixedIsMin, Point  min, Point  max);

    [[nodiscard]] u_short fixedDim() const;
    [[nodiscard]] bool fixedIsMin() const;
    [[nodiscard]] const Point& min1() const;
    [[nodiscard]] const Point& max1() const;

    [[nodiscard]] std::array<Point, 4> points() const;
    [[nodiscard]] std::array<LineSegment, 4> lineSegments() const;
    [[nodiscard]] std::optional<RayIntersection> intersection(const Ray& ray) const;
    [[nodiscard]] std::optional<RayIntersection> intersectionInclusive(const Ray& ray) const;
    [[nodiscard]] std::optional<Point> intersectionPointInclusive(const LineSegment& ls) const;

  private:
    u_short _fixedDim;
    bool _fixedIsMin;
    Point _min;
    Point _max;
  };
}
