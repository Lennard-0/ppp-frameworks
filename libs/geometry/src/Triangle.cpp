//
// Created by lennard on 10/15/20.
//

#include "geometry/Triangle.h"
#include <optional>

namespace gb {

  Triangle::Triangle() = default;
  Triangle::Triangle(const Point& pointA, const Point& pointB, const Point& pointC)
  : _pointA(pointA)
  , _pointB(pointB)
  , _pointC(pointC)
  {}

  const Point& Triangle::pointA() const {
    return _pointA;
  }

  const Point& Triangle::pointB() const {
    return _pointB;
  }

  const Point& Triangle::pointC() const {
    return _pointC;
  }

  std::array<Point, 3> Triangle::points() const {
    return {_pointA, _pointB, _pointC};
  }

  std::array<LineSegment, 3> Triangle::lineSegments() const { //todo: kann die funktion weg?
    return {{
      {_pointA, _pointB},
      {_pointB, _pointC},
      {_pointC, _pointA}
    }};
  }

  Plane Triangle::plane() const {
    return {_pointA, _pointB, _pointC};
  }

  std::optional<fp_t> Triangle::intersectionLambdaInclusive(const Ray& ray) const {
    auto pts = points();

    Vector edge1, edge2, h, s, q;
    fp_t a,f,u,v;
    edge1 = pts[1] - pts[0];
    edge2 = pts[2] - pts[0];
    h = ray.direction().cross(edge2);
    a = edge1.dot(h);
    if (a > -EPSILON && a < EPSILON)
      return {};   //ray is parallel to the triangle
    f = 1.0/a;
    s = ray.origin() - pts[0];
    u = f * s.dot(h);
    if (u < 0.0 || u > 1.0)
      return {};
    q = s.cross(edge1);
    v = f * ray.direction().dot(q);
    if (v < 0.0 || u + v > 1.0)
      return {};
    // At this stage we can compute lambda to find out where the intersection point is on the line.
    float lambda = f * edge2.dot(q);

    if (lambda < -EPSILON)
      return {}; // intersection behind origin
    return {lambda};
  }
}