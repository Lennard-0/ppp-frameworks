//
// Created by lennard on 10/15/20.
//

#include "geometry/Point.h"
#include "geometry/Vector.h"

namespace gb {

  inline bool operator==(const Point& lhs, const Point& rhs) {
    return lhs.equals(rhs);
  }

  Point operator+(const Point& lhs, const Vector& rhs) {
    Point result;
    result[0] = lhs[0] + rhs[0];
    result[1] = lhs[1] + rhs[1];
    result[2] = lhs[2] + rhs[2];
    return result;
  }
  Vector operator-(const Point& lhs, const Point& rhs) {
    Vector result;
    result[0] = lhs[0] - rhs[0];
    result[1] = lhs[1] - rhs[1];
    result[2] = lhs[2] - rhs[2];
    return result;
  }

  Point middle(const Point& lhs, const Point& rhs) {
    Point result;
    result[0] = 0.5*lhs[0] + 0.5*rhs[0];
    result[1] = 0.5*lhs[1] + 0.5*rhs[1];
    result[2] = 0.5*lhs[2] + 0.5*rhs[2];
    return result;
  }

}