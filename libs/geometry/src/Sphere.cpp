//
// Created by lennard on 10/15/20.
//

#include "geometry/Sphere.h"

#include <utility>

namespace gb {
  Sphere::Sphere()
  : _radius(0)
  {};
  Sphere::Sphere(Point  center, const fp_t& radius)
  : _center(std::move(center))
  , _radius(radius)
  {}

  const Point& Sphere::center() const {
    return _center;
  }

  fp_t Sphere::radius() const {
    return _radius;
  }

}