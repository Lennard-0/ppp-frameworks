//
// Created by lennard on 10/15/20.
//

#include "geometry/Tetrahedron.h"
#include <set>
#include <utility>
#include <algorithm> //for remove_if

namespace gb {
  Tetrahedron::Tetrahedron() = default;
  Tetrahedron::Tetrahedron(std::array<Point, 4>  points)
  : _p(std::move(points))
  {}
  Tetrahedron::Tetrahedron(Point p0, Point p1, Point  p2, Point p3)
  : _p{std::move(p0), std::move(p1), std::move(p2), std::move(p3)}
  {}

  const std::array<Point, 4>& Tetrahedron::points() const {
    return _p;
  }

  std::array<LineSegment, 6> Tetrahedron::lineSegments() const {
    return {{
      {_p[1], _p[2]},
      {_p[2], _p[3]},
      {_p[3], _p[1]},
      {_p[0], _p[1]},
      {_p[3], _p[0]},
      {_p[2], _p[0]}
    }};
  }

  std::array<Triangle, 4> Tetrahedron::triangles() const {
    return {{
      {_p[1],_p[2],_p[3]},
      {_p[0],_p[3],_p[2]},
      {_p[0],_p[1],_p[3]},
      {_p[0],_p[2],_p[1]}
    }};
  }

  bool Tetrahedron::containsInInterior(const Point& point) const {
    for(auto const& triangle : triangles())
      if(!triangle.plane().isLeft(point))
        return false;
    return true;
  }

  bool Tetrahedron::intersects(const AABox& aaBox) const {
    for(auto const& point : _p)
      if(aaBox.containsInInterior(point))
        return true;

    for(auto const& aaRec : aaBox.facets())
      if(intersects(aaRec))
        return true;

    return false;
  }

  bool Tetrahedron::intersects(const AARectangle& aaRec) const {
    for(auto const& point : aaRec.points())
      if(containsInInterior(point))
        return true;
    std::set<Point, Point::lex_compare> iPoints;
    for(auto const& aaRecLS : aaRec.lineSegments())
      for(auto const& iPoint : intersectionPointsInclusive(aaRecLS))
        iPoints.insert(iPoint);

    if(iPoints.size() >= 2)
      return true;
    iPoints = {};
    for(auto const& tetraLS : lineSegments()) {
      auto iPoint = aaRec.intersectionPointInclusive(tetraLS);
      if(iPoint)
        iPoints.insert(iPoint.value());
    }

    return iPoints.size() >= 2;
  }

  bool Tetrahedron::intersects(const Ray& ray) const {
    return ! intersectionLambdasInclusive(ray).empty();
  }

  std::vector<fp_t> Tetrahedron::intersectionLambdasInclusive(const Ray& ray) const {
    std::set<fp_t> result;

    if(containsInInterior(ray.origin()))
      result.insert(0);

    for(auto const& triangle : triangles()) {
      auto facetIntersecLambda = triangle.intersectionLambdaInclusive(ray);
      if (facetIntersecLambda)
        result.insert(facetIntersecLambda.value());
    }

    return result.empty() ? std::vector<fp_t>{} : std::vector<fp_t>{result.begin(), result.end()};
  }

  std::vector<fp_t> Tetrahedron::intersectionLambdasInclusive(const LineSegment& ls) const {
    auto lambdas = intersectionLambdasInclusive(ls.lsRay());                   // inclusive regarding Tetra
    auto it = std::remove_if(lambdas.begin(), lambdas.end(),                 // todo: differentiate between these two
                             [](const fp_t& lambda) { return lambda > 1; });  // inclusive regarding lineSegment
    lambdas.erase(it, lambdas.end());
    return lambdas;
  }

  std::vector<Point> Tetrahedron::intersectionPointsInclusive(const LineSegment& ls) const {
    return ls.lsRay().at(intersectionLambdasInclusive(ls));
  }
}