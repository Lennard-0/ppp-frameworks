//
// Created by lennard on 10/16/20.
//

#include "grid/Grid.h"

#include <filesystem>
#include <fstream>
#include <OpenEXR/half.h>
#include <cstring>

namespace gb {

  size_t Grid::size() const {
    return _size;
  }

  const std::vector<Tetrahedron>& Grid::tetrahedrons() const {
    return _tetrahedrons;
  }

  const Tetrahedron* Grid::tetrahedronsPtr() const {
    return _tetrahedronsPtr;
  }

  const Tetrahedron& Grid::tetrahedron(const PrimitiveID& id) const {
    return _tetrahedrons[id];
  }

  const std::vector<float>& Grid::data() const {
    return _data;
  }

  const float* Grid::dataPtr() const {
    return _dataPtr;
  }

  float Grid::data(const PrimitiveID& id) const {
    return _data[id];
  }

  AABox Grid::aabb() const {
    return _aabb;
  }

  void Grid::loadFromFile(const std::filesystem::path& filePath) {
    std::ifstream is(filePath, std::ios::binary);
    if (is.fail())
      throw std::runtime_error("errno=\"" + std::string(strerror(errno)) + "\": " + filePath.string());

    unsigned long long i = 0;
    unsigned int numEdgePrimitives;

    is.read(reinterpret_cast<char*>(&numEdgePrimitives), sizeof(numEdgePrimitives));
    std::cout << "debug: " << "num_edge_tetras: " << numEdgePrimitives;
    is.ignore(15 * sizeof(unsigned int));

    while (true) {
      std::array<Point,4> points;

      std::array<std::array<fp_t, 3>, 4> pointsBuffer{};
      is.read(reinterpret_cast<char*>(&pointsBuffer), sizeof(pointsBuffer));

      for(int j=0; j < 4; j++)
        points[j] = Point(pointsBuffer[j]);

      std::array<u_int32_t, 4> payload{};
      is.read(reinterpret_cast<char*>(&payload), sizeof(payload));

//      Neighbors neighbors;
//      neighbors[0].setPrimitiveID( 0x00FFFFFF & (payload[0] >> 8) );
//      neighbors[1].setPrimitiveID( (0x00FF0000 & (payload[0] << 16)) | (0x0000FFFF & (payload[1] >> 16)) );
//      neighbors[2].setPrimitiveID( (0x00FFFF00 & (payload[1] << 8)) | (0x000000FF & (payload[2] >> 24)) );
//      neighbors[3].setPrimitiveID( 0x00FFFFFF & payload[2] );
//
//      neighbors[0].setFacetID( 0x00000003 & (payload[3] >> 30) );
//      neighbors[1].setFacetID( 0x00000003 & (payload[3] >> 28) );
//      neighbors[2].setFacetID( 0x00000003 & (payload[3] >> 26) );
//      neighbors[3].setFacetID( 0x00000003 & (payload[3] >> 24) );

      half h;
      h.setBits(0x0000FFFF & payload[3]);

      if (!is.eof()) {
        Tetrahedron tetra(points);
//        TetrahedronPrimitive tetraPrimitive(tetra, neighbors, (fp_t) h);
//        _primitives.emplace_back(tetraPrimitive);
        _tetrahedrons.emplace_back(tetra);
        _data.emplace_back((float) h);
        ++i;
      }
      else
        break;
    }
    _size = i;
    _tetrahedronsPtr = &_tetrahedrons[0];
    _dataPtr = &_data[0];

    calculateAABB();

    std::cout << "info: " << "Read " << _size << " tetrahedrons from disk." << std::endl;
    _filePath = std::filesystem::absolute(filePath);
  }

  std::string Grid::filePath() const {
    return _filePath;
  }

  void Grid::calculateAABB() {
    Point min = {INFINITY, INFINITY, INFINITY};
    Point max = {-INFINITY, -INFINITY, -INFINITY};
    //todo
    for(auto const& tetrahedron : _tetrahedrons)
      for(auto const& point : tetrahedron.points()) {
        if(point[0] < min[0]) min[0] = point[0];
        if(point[1] < min[1]) min[1] = point[1];
        if(point[2] < min[2]) min[2] = point[2];
        if(point[0] > max[0]) max[0] = point[0];
        if(point[1] > max[1]) max[1] = point[1];
        if(point[2] > max[2]) max[2] = point[2];
      }
    _aabb = {min, max};
  }

}