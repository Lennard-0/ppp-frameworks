//
// Created by lennard on 10/14/20.
//

#include "OpenglWindow.h"

static void glfw_error_callback(int error, const char* description)
{
  std::cerr << "Error(" << error << "): " << description << '\n';
}

OpenglWindow::OpenglWindow()
  : _width(260)
  , _height(180)
{}

void OpenglWindow::create(){
  glfwSetErrorCallback(glfw_error_callback);
  if (!glfwInit()) exit(EXIT_FAILURE);

  _window = glfwCreateWindow(_width, _height, "Gridbox", nullptr, nullptr);
  if (!_window)  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  glfwMakeContextCurrent(_window);
  glfwSwapInterval(0); // aka vsync on(1)/off(0)

  if (glewInit() != GLEW_OK) exit(EXIT_FAILURE);
}
[[nodiscard]] uint OpenglWindow::width() const {
  return _width;
}
[[nodiscard]] uint OpenglWindow::height() const {
  return _height;
}
double OpenglWindow::aspectRatio() const {
  return (double) _width / (double) _height;
}
bool OpenglWindow::shouldClose() {
  return glfwWindowShouldClose(_window);
}
void OpenglWindow::finalizeFrame() {
  glfwSwapBuffers(_window);
  glfwPollEvents();
}
void OpenglWindow::close() {
  glfwDestroyWindow(_window);
  glfwTerminate();
}