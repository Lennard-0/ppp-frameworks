//
// Created by lennard on 10/14/20.
//

#include "OpenglBuffer.h"

OpenglBuffer::OpenglBuffer() = default;
OpenglBuffer::OpenglBuffer(const uint& width, const uint& height)
  : _width(width)
  , _height(height)
{}
OpenglBuffer::~OpenglBuffer() {
  if(_created)
    glDeleteBuffers(1, &_id);
}
void OpenglBuffer::create() {
  glGenBuffers(1, &_id);
  glBindBuffer(GL_PIXEL_UNPACK_BUFFER, _id);
  glBufferData(GL_PIXEL_UNPACK_BUFFER, 4* sizeof(GLubyte)*_width*_height, nullptr, GL_DYNAMIC_DRAW);
  _created = true;
}
[[nodiscard]] GLuint OpenglBuffer::id() const {
  return _id;
}
void OpenglBuffer::drawToFrame() {
  glDrawPixels(_width, _height, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
}

uint OpenglBuffer::width() const {
  return _width;
}

uint OpenglBuffer::height() const {
  return _height;
}