//
// Created by lennard on 11/1/20.
//

#include "scene/Camera.h"

namespace gb {


  const Point& Camera::position() const {
    return _position;
  }
  const Vector& Camera::direction() const {
    return _direction;
  }

  ENABLE_DEVICE
  float Camera::fov() const {
    return _fov;
  }
  float Camera::verticalFOV() const {
    return _verticalFOV;
  }
  float Camera::aspectRatio() const {
    return _aspectRatio;
  }

  void Camera::setPosition(const Point& position) {
    _position = position;
  }
  void Camera::setDirection(const Vector& direction) {
    _direction = direction;
  }

  void Camera::setFov(float fov) {
    _fov = fov;
    updateVerticalFOV();
  }
  void Camera::setAspectRatio(float aspectRatio) {
    _aspectRatio = aspectRatio;
    updateVerticalFOV();
  }

  void Camera::targetFullFrame(const AABox& aaBox) {
    auto boxMid = aaBox.mid();
    auto sphereR = (aaBox.max() - boxMid).norm2();
    auto distanceToMid = tt::cotDegree(_fov / 2) * sphereR;

//    _position = boxMid + Vector(0, distanceToMid, 0);//aka projection point
//    Vector cameraDirection = {0, -1, 0};
    _position = boxMid + Vector(0, 0, distanceToMid);//aka projection point
    Vector cameraDirection = {0, 0, -1};
  }

  void Camera::updateVerticalFOV() {
    double focalLength = 10;
    double screenWidth = tan((_fov / 2) * M_PI / 180.0) * focalLength * 2;//aka width of projection plane/viewport
    double screenHeight = screenWidth / _aspectRatio;
    _verticalFOV = (atan((screenHeight / 2.0) / focalLength) * 180.0 / M_PI) * 2;
    std::cout << "info: " << " virtual screen: " << screenWidth << "x" << screenHeight
              << " FOV: " << _fov << "/" << _verticalFOV << " (horizontal/vertical)\n";
  }


}