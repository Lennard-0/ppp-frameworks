//
// Created by lennard on 3/10/21.
//

#include "tt.h"

std::ostream& operator<<(std::ostream& o, std::chrono::nanoseconds ns) {

  auto m = std::chrono::duration_cast<std::chrono::minutes>(ns);
  ns -= m;
  auto s = std::chrono::duration_cast<std::chrono::seconds>(ns);
  ns -= s;
  auto ms = std::chrono::duration_cast<std::chrono::milliseconds>(ns);
  ns -= ms;
  auto us = std::chrono::duration_cast<std::chrono::microseconds>(ns);
  ns -= us;

  if(m.count())
    o << m.count() << "m ";
  if(s.count())
    o << s.count() << "s ";
  if(ms.count())
    o << ms.count() << "ms ";
  o << us.count() << "us";

  return o;
}

namespace tt {

  std::string bytesToReadableStr(u_long bytes) {

    auto tb = bytes / pow(1024, 4);
    bytes -= tb * pow(1024, 4);
    auto gb = bytes / pow(1024, 3);
    bytes -= gb * pow(1024, 3);
    auto mb = bytes / pow(1024, 2);
    bytes -= mb * pow(1024, 2);
    auto kb = bytes / 1024;
    bytes -= kb * 1024;

    std::string result;
    u_short n = 0;
    if(tb > 0) {
      result += std::to_string(tb) + "TiB ";
      n++;
    }
    if(n==1 || gb>0) {
      result += std::to_string(gb) + "GiB ";
      n++;
    }
    if(n==1 || mb>0) {
      result += std::to_string(mb) + "MiB ";
      n++;
    }
    if(n==1 || kb>0) {
      result += std::to_string(kb) + "KiB ";
      n++;
    }
    if(n<2) {
      result += std::to_string(bytes) + "B";
    }
    return result;
  }
}