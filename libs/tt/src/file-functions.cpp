//
// Created by lennard on 3/12/21.
//

#include "tt/file-functions.h"
#include "tt/string-functions.h"
#include <iterator>
#include <fstream>


namespace tt {

  std::vector<std::string> readWordsFromFile(const std::filesystem::path& filePath) {
    if(!std::filesystem::is_regular_file(filePath))
      throw std::runtime_error(std::string("error in ") + __PRETTY_FUNCTION__ + ": " + filePath.string() + " is not a regular file");
    std::ifstream file(filePath);
    if(!file)
      throw std::runtime_error("could not read file" + filePath.string());

    std::vector<std::string> fileContents;
    std::copy(std::istream_iterator<std::string>(file),
              std::istream_iterator<std::string>(),
              std::back_inserter(fileContents));

    return fileContents;
  }

  std::vector<std::string> readLinesFromFile(const std::filesystem::path& filePath) {
    if(!std::filesystem::is_regular_file(filePath))
      throw std::runtime_error(std::string("error in ") + __PRETTY_FUNCTION__ + ": " + filePath.string() + " is not a regular file");
    std::ifstream file(filePath);
    if(!file)
      throw std::runtime_error("could not read file" + filePath.string());

    std::vector<std::string> fileContents;
    std::string line;
    while(std::getline(file, line))
      fileContents.emplace_back(line);

    return fileContents;
  }

  std::vector<std::vector<std::string>> readRowsFromFile(const std::filesystem::path& filePath) {
    if(!std::filesystem::is_regular_file(filePath))
      throw std::runtime_error(std::string("error in ") + __PRETTY_FUNCTION__ + ": " + filePath.string() + " is not a regular file");

    std::vector<std::vector<std::string>> result;
    for(const std::string& line : readLinesFromFile(filePath))
      result.emplace_back(splitIntoWords(line));
    return result;
  }

  std::string readFileToString(const std::filesystem::path& filePath) {
    if (!std::filesystem::is_regular_file(filePath))
      throw std::runtime_error(
        std::string("error in ") + __PRETTY_FUNCTION__ + ": " + filePath.string() + " is not a regular file");
    std::fstream kernelFile(filePath);
    std::string content(
      (std::istreambuf_iterator<char>(kernelFile)),
      std::istreambuf_iterator<char>()
    );
    return content;
  }
}