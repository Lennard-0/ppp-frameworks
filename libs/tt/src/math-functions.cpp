//
// Created by lennard on 3/10/21.
//

#include "tt.h"

namespace tt {

  //todo: ab c++20 std::numbers::pi statt M_PI
//  constexpr float toRadian(const float& angleDegree) {
  float toRadian(const float& angleDegree) {
    return angleDegree * (float)M_PI / 180.0f;
  }
//  constexpr float toDegree(const float& angleRadian) {
  float toDegree(const float& angleRadian) {
    return angleRadian * 180.0f / (float)M_PI;
  }

  inline double cot(const double& angleRadian) {
    return cos(angleRadian) / sin(angleRadian);
  }

  double cotDegree(const double& angleDegree) {
    return cot(toRadian(angleDegree));
  }

}