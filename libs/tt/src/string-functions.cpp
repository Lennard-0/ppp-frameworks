//
// Created by lennard on 3/12/21.
//

#include "tt/string-functions.h"
#include <sstream>
#include <iterator>

namespace tt {

  std::vector<std::string> splitIntoWords(const std::string& str) {
    std::istringstream iss(str);
    return {
      std::istream_iterator<std::string>{iss},
      std::istream_iterator<std::string>{}
    };
  }
}