//
// Created by lennard on 3/12/21.
//

#pragma once

#include <vector>
#include <string>
#include <filesystem>

namespace tt {

  std::vector<std::string> readWordsFromFile(const std::filesystem::path& filePath);
  std::vector<std::string> readLinesFromFile(const std::filesystem::path& filePath);
  std::vector<std::vector<std::string>> readRowsFromFile(const std::filesystem::path& filePath);
  std::string readFileToString(const std::filesystem::path& filePath);

}