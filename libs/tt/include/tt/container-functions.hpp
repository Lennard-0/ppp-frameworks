//
// Created by lennard on 4/4/21.
//

#pragma once

#include <set>
#include <map>

namespace tt {

  ///@brief returns a set filled with consecutive elements
  template<typename T>
  std::set<T> iotaSet(const T& start, const T& end) {
    std::set<T> result;
    for(T i=start; i<end; i++)
      result.insert(i);
    return result;
  }

  template<typename K, typename V>
  std::set<K> keys(const std::map<K,V>& map) {
    std::set<K> result;
    for(auto const& el : map)
      result.emplace(el.first);
    return result;
  }

  template<typename K, typename V>
  std::vector<V> values(const std::map<K,V>& map) {
    std::vector<V> result;
    for(auto const& el : map)
      result.emplace_back(el.second);
    return result;
  }
}