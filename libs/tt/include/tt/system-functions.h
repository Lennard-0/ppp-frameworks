//
// Created by lennard on 3/12/21.
//

#pragma once

#include <sys/types.h>
#include <chrono>

namespace tt {

  ulong ownVSizeInBytes();

  std::chrono::nanoseconds elapsedTime();
}