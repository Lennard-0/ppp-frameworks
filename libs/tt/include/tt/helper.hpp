//
// Created by lennard on 3/11/21.
//

#pragma once

template<class T> void _unused( const T& ) { }
void unused();
template<typename Arg1, typename... Args>
void unused(const Arg1& arg1, const Args&... args)
{
  _unused( arg1 );
  unused(args...);
}