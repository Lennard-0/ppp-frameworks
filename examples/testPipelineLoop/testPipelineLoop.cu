

#include "testPipelineLoop.h"
#include <cf/Pipeline.hpp>


void testPipeline() {

// x----1
//     / \
// y--0   3--s
//     \ /
//      2
//
// s = <x+ay, scan(ay,+)>
// 0: h = a*y
// 1: t = x+h
// 2: k = scan(h,+)
// 3: s = <t, k>

  size_t N = 1<<26;

  double a = 2;
  std::vector<double> x(N);
  std::vector<double> y(N);

  cf::ArrayHandler<double> x_ah(x);
  cf::ArrayHandler<double> y_ah(y);
  cf::ArrayHandler<double> h_ah(N);
  cf::ArrayHandler<double> t_ah(N);
  cf::ArrayHandler<double> k_ah(N);

  cf::Pipeline pipeline;

  //Node 0
  pipeline.addNode([&](cf::Node& node){
    auto y_dvc = y_ah.readView(node);
    auto h_dvc = h_ah.writeView(node);

    node.forall(N, CF_LAMBDA(size_t i) {
      h_dvc[i] = a * y_dvc[i];
    });
  }, cf::GPU);

  //Node 1
  pipeline.addNode([&](cf::Node& node){
    auto x_dvc = x_ah.readView(node);
    auto h_dvc = h_ah.readView(node);
    auto t_dvc = t_ah.writeView(node);

    node.forall(N, CF_LAMBDA(size_t i) {
      t_dvc[i] = x_dvc[i] + h_dvc[i];
    });
  }, cf::GPU);

  //Node 2
  pipeline.addNode([&](cf::Node& node){
    node.scan(k_ah, h_ah, (double)0, cf::Add<double>{});
  }, cf::GPU);

  double result = 0;
//  cf::Mutex resultMutex;
  //Node 3
  pipeline.addNode([&](cf::Node& node){
    auto t_dvc = t_ah.readView(node);
    auto k_dvc = k_ah.readView(node);
//    node.require(resultMutex);

    auto inputFunction = CF_LAMBDA(size_t i)->double { return t_dvc[i] * k_dvc[i]; };
    node.reduce(result, inputFunction, cf::Add<double>{}, N);
  }, cf::GPU);



  for(int i=0; i<2; i++) {

    pipeline.writable(x_ah).wait();
    pipeline.writable(y_ah).wait();

    double xValue = i+2;
    double yValue = i+1;
    std::fill(x.begin(), x.end(), xValue);
    std::fill(y.begin(), y.end(), yValue);

    pipeline.runAsync();

//    pipeline.runAsync([&result,i](){
//      std::cout << "executed pipeline " << i << '\n';
//
//      // expected = <x+ay, scan(ay,+)>
//      double iotaSum = (double)(N*N + N) / 2.f;
//      double expected = (xValue + a*yValue)*(a*yValue*iotaSum);
//
//      std::cout << std::scientific;
//      std::cout << "expected = " << expected << " (" << std::fixed << expected << ")\n" << std::scientific;
//      std::cout << "result   = " << result << " (" << std::fixed << result << ")\n" << std::scientific;
//
//      if(result != expected)
//        std::cerr << "failure: wrong result\n";
//      else
//        std::cout << "success!\n";
//    }, resultMutex);
  }

  pipeline.wait();
}