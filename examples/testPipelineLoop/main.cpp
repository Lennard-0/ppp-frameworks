
#include "testPipelineLoop.h"
#include <iostream>
#include <tt.h>
#include <algorithm>

int main() {

  auto t0 = std::chrono::steady_clock::now();
  testPipeline();
  auto t1 = std::chrono::steady_clock::now();
  std::cout << "execution time: " << t1-t0 << '\n';

	return 0;
}
