
#include "testPipeline.h"
#include <iostream>
#include <tt.h>
#include <algorithm>
#include <fstream>

int main() {
	
	size_t N = 1<<26;
  using ValueT = double;
	ValueT xValue = 2;
	ValueT yValue = 1;
	ValueT a = 2;

  std::vector<ValueT> x(N, xValue);
	std::vector<ValueT> y(N, yValue);

  auto t0 = std::chrono::steady_clock::now();
  ValueT result = testPipeline(a, x, y);
  auto t1 = std::chrono::steady_clock::now();
  std::cout << "execution time: " << t1-t0 << '\n';

  // expected = <x+ay, scan(ay,+)>
  ValueT iotaSum = (ValueT)(N*N + N) / 2.f;
  ValueT expected = (xValue + a*yValue)*(a*yValue*iotaSum);

  std::cout << std::scientific;
  std::cout << "expected = " << expected << " (" << std::fixed << expected << ")\n" << std::scientific;
  std::cout << "result   = " << result << " (" << std::fixed << result << ")\n" << std::scientific;

  if(result != expected) {
    std::cerr << "failure: wrong result\n";
    return -1;
  }
	std::cout << "success!\n";

	return 0;
//
//  int N = 1<<28;
//
//  std::vector<float> y(N, 1);
//  std::vector<float> x(N, 2);
//
//  auto t0 = std::chrono::steady_clock::now();
//  saxpy(2,x,y);
//  auto t1 = std::chrono::steady_clock::now();
//  std::cout << "execution time: " << t1-t0 << std::endl;
//
//  std::ofstream outfile;
//  outfile.open("times.txt", std::ios_base::app);
//  outfile << (long) std::chrono::duration<double,std::milli>(t1-t0).count() << '\n';
//
//  for(auto const& el : y)
//    if(el != 5) {
//      std::cerr << "failure: wrong result\n" << el;
//      return -1;
//    }
//
//  std::cout << "success!\n";
//
//  return 0;
}
