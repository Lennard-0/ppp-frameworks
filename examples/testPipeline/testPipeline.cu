

#include "testPipeline.h"
#include <cf/Pipeline.hpp>


double testPipeline(double a, const std::vector<double>& x, const std::vector<double>& y) {

// x----1
//     / \
// y--0   3--s
//     \ /
//      2
//
// s = <x+ay, scan(ay,+)>
// 0: h = a*y
// 1: t = x+h
// 2: k = scan(h,+)
// 3: s = <t, k>

  size_t N = x.size();
  size_t xN = x.size();

  cf::ArrayHandler<double> x_ah(x);
  cf::ArrayHandler<double> y_ah(y);
  cf::ArrayHandler<double> t_ah(xN);
  cf::ArrayHandler<double> k_ah(N);

  cf::Pipeline pipeline;

  pipeline.addNode([&](cf::Node& node){
    cf::scan(t_ah, x_ah, target, (double)0, cf::Add<double>{});
  });

  double rT = 0;
  pipeline.addNode([&](cf::Node& node){
    auto t_dvc = t_ah.readView(target);
    cf::reduce(rT, CF_LAMBDA(size_t i)->double { return t_dvc[i]; }, cf::Add<double>{}, xN, target);
  });

  pipeline.addNode([&](cf::Node& node){
    cf::scan(k_ah, y_ah, target, (double)0, cf::Add<double>{});
  });

  double rY = 0;
  pipeline.addNode([&](cf::Node& node){
    auto k_dvc = k_ah.readView(target);
    cf::reduce(rY, CF_LAMBDA(size_t i)->double { return k_dvc[i]; }, cf::Add<double>{}, N, target);
  });

  double result;
  pipeline.addNode([&](cf::Node& node){

    node.forall(1, [&](size_t i) {
      result = rT+rY;
    });
  });

//  cf::ArrayHandler<double> x_ah(x);
//  cf::ArrayHandler<double> y_ah(y);
//  cf::ArrayHandler<double> h_ah(N);
//  cf::ArrayHandler<double> t_ah(N);
//  cf::ArrayHandler<double> k_ah(N);
//
//  cf::Pipeline pipeline;
//
//  //Node 0
//  pipeline.addNode([&](cf::Node& node){
//    auto y_dvc = y_ah.readView(node);
//    auto h_dvc = h_ah.writeView(node);
//
//    node.forall(N, CF_LAMBDA(size_t i) {
//      h_dvc[i] = a * y_dvc[i];
//    });
//  }, cf::GPU);
//
//  //Node 1
//  pipeline.addNode([&](cf::Node& node){
//    auto x_dvc = x_ah.readView(node);
//    auto h_dvc = h_ah.readView(node);
//    auto t_dvc = t_ah.writeView(node);
//
//    node.forall(N, CF_LAMBDA(size_t i) {
//      t_dvc[i] = x_dvc[i] + h_dvc[i];
//    });
//  }, cf::GPU);
//
//  //Node 2
//  pipeline.addNode([&](cf::Node& node){
//    node.scan(k_ah, h_ah, (double)0, cf::Add<double>{});
//  }, cf::GPU);
//
//  double result = 0;
//  //Node 3
//  pipeline.addNode([&](cf::Node& node){
//    auto t_dvc = t_ah.readView(node);
//    auto k_dvc = k_ah.readView(node);
//
//    auto inputFunction = CF_LAMBDA(size_t i)->double { return t_dvc[i] * k_dvc[i]; };
//    node.reduce(result, inputFunction, cf::Add<double>{}, N);
//  }, cf::GPU);

  pipeline.run();

  return result;
}

void saxpy(float a, const std::vector<float>& x, std::vector<float>& y) {

  size_t N = x.size();

  cf::ArrayHandler<float> x_ah(x);
  cf::ArrayHandler<float> y_ah(y);

  cf::Pipeline pipeline;

  //Node 0
  pipeline.addNode([&](cf::Node& node){
    auto x_dvc = x_ah.readView(node);
    auto y_dvc = y_ah.readWriteView(node);

    node.forall(N, CF_LAMBDA(size_t i) {
      y_dvc[i] = a * x_dvc[i] + y_dvc[i];
  });
  }, cf::GPU);

  pipeline.run();
}