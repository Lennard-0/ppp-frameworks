//
// Created by lennard on 5/5/21.
//

#include "saxpyy.h"

void saxpyy(float a, const std::vector<float>& x, std::vector<float>& y) {

  size_t N = x.size();

  ArrayHandler<float> x_ah(x);
  ArrayHandler<float> y_ah(y);
  ArrayHandler<float> t_ah(N);

  assign(cpu,
         y_ah,
         mul(assign(cpu,
                    t_ah,
                    add(y_ah,
                        muls(a,
                             x_ah
                        )
                    )
             ),
             y_ah
         )
  ).wait();

}