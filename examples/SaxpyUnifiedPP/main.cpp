
#include "saxpy.h"
#include <iostream>
#include <tt.h>
#include <fstream>

int main() {

	int N = 1<<28;
	
	std::vector<float> y(N, 1);
	std::vector<float> x(N, 2);

  auto t0 = std::chrono::steady_clock::now();
  saxpy(2,x,y);
  auto t1 = std::chrono::steady_clock::now();
  std::cout << "execution time: " << t1-t0 << std::endl;

  std::ofstream outfile;
  outfile.open("times.txt", std::ios_base::app);
  outfile << (long) std::chrono::duration<double,std::milli>(t1-t0).count() << '\n';

  for(auto const& el : y)
		if(el != 5) {
			std::cerr << "failure: wrong result\n" << el;
			return -1;
		}
	
	std::cout << "success!\n";
	
	return 0;
}
