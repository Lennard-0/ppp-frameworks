
set(saxpy-sources main.cpp saxpy.h saxpy.cpp)
set(saxpy-sources-cuda main.cpp saxpy.h saxpy.cu)
set(saxpy-sources-sycl saxpy.cpp)

if(USE_CUDA OR USE_OPENMP)
  enable_language(CUDA)

  add_executable(SaxpyUnifiedPP ${saxpy-sources-cuda})
  target_link_libraries(SaxpyUnifiedPP PUBLIC unifiedPP)
  target_link_libraries(SaxpyUnifiedPP PUBLIC TurtleTools)

  target_compile_options(SaxpyUnifiedPP PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
  target_compile_options(SaxpyUnifiedPP PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:-lineinfo>)#todo: only in debug?
  target_compile_options(SaxpyUnifiedPP PRIVATE $<$<COMPILE_LANGUAGE:CUDA>:--extended-lambda -Xcompiler -fopenmp>)
  target_compile_features(SaxpyUnifiedPP PRIVATE cxx_std_17)
  set_target_properties(
      SaxpyUnifiedPP
      PROPERTIES
      CUDA_SEPARABLE_COMPILATION ON)
#elseif(USE_OPENMP)
#  add_executable(SaxpyUnifiedPP ${saxpy-sources})
#  target_link_libraries(SaxpyUnifiedPP PUBLIC unifiedPP TurtleTools)
elseif(USE_OPENCL)
  add_executable(SaxpyUnifiedPP ${saxpy-sources})
  target_link_libraries(SaxpyUnifiedPP PUBLIC unifiedPP TurtleTools OpenCL::OpenCL)
elseif(USE_KOKKOS)
  add_executable(SaxpyUnifiedPP ${saxpy-sources})
  target_link_libraries(SaxpyUnifiedPP PUBLIC unifiedPP TurtleTools Kokkos::kokkos)
  kokkos_check(OPTIONS CUDA_LAMBDA)
elseif(USE_RAJA)
  blt_add_executable(
      NAME SaxpyUnifiedPP
      SOURCES ${saxpy-sources}
      DEPENDS_ON ${raja-depends} unifiedPP TurtleTools
      DEFINES ${raja-defines}
      OUTPUT_DIR ${CMAKE_CURRENT_BINARY_DIR})
elseif(USE_SYCL)
  add_executable(SaxpyUnifiedPP ${saxpy-sources})
  target_link_libraries(SaxpyUnifiedPP PUBLIC unifiedPP TurtleTools)
  add_sycl_to_target(TARGET SaxpyUnifiedPP SOURCES ${saxpy-sources-sycl})
elseif(USE_OPENMP-TARGET)
  add_executable(SaxpyUnifiedPP ${saxpy-sources})
  target_link_libraries(SaxpyUnifiedPP PUBLIC unifiedPP TurtleTools)
else()
  message(FATAL_ERROR "no target backend enabled")
endif()
