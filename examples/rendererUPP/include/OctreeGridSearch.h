//
// Created by lennard on 3/10/21.
//

#pragma once

#include <grid/Grid.h>
#include <OpenglBuffer.h>
#include <scene/Camera.h>
#include <unifiedPP.hpp>
#include <memory>

class OctreeGridSearch {
public:
  explicit OctreeGridSearch(uPP::Context& ctx);                                             // Constructor
  OctreeGridSearch(const OctreeGridSearch&) = delete;             // Copy constructor
  OctreeGridSearch(OctreeGridSearch&&) = delete;                  // Move constructor
  OctreeGridSearch& operator=(const OctreeGridSearch&) = delete;  // Copy assignment operator
  OctreeGridSearch& operator=(OctreeGridSearch&&) = delete;       // Move assignment operator
  ~OctreeGridSearch();                                            // Destructor

  void construct(const gb::Grid& grid, u_long maxRegionSize);
  void setOutputBuffer(const OpenglBuffer& openglBuffer);
  void render(const gb::Camera& camera);

public://avoid error: An extended __device__ lambda cannot be defined inside a class ("Impl") with private or protected access within another class
  struct Impl;
private:
  std::unique_ptr<Impl> _impl;

};