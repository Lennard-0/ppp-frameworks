//
// Created by lennard on 3/10/21.
//

#include "Octree.h"

bool Octree::isLeaf() const {
  return regionStart != gb::INVALID_PRIMITIVE_ID;
}
