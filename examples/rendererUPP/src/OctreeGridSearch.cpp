//
// Created by lennard on 3/10/21.
//

#include "OctreeGridSearch.h"
#include "Octree.h"
#include <PixelRGBA.h>
#include <unifiedPP.hpp>

using namespace gb;

struct OctreeGridSearch::Impl {
  Impl(uPP::Context& ctx)
  : _ctx(ctx)
  {}

  void construct(const gb::Grid& grid, u_long maxRegionSize) {
    _nTetras = grid.size();
    _maxRegionSize = maxRegionSize;

    _tetras_host = uPP::Array<Tetrahedron>(grid.tetrahedrons());
    _tetras_dvc = _tetras_host.deviceAccess(uPP::transfer::to_const, _ctx);

    _data_host = uPP::Array<fp_t>(grid.data());
    _data_dvc = _data_host.deviceAccess(uPP::transfer::to_const, _ctx);

    std::vector<PrimitiveID> ids(_nTetras);
    std::iota(ids.begin(), ids.end(), 0);

    Octree root;
    root.aaBox = grid.aabb();
    root.regionSize = _nTetras;
    _octreeNodes.emplace_back(root);

    _regions.reserve(_nTetras*3);

    split(_octreeNodes[0], _regions, _octreeNodes, ids);

    _regions.shrink_to_fit();
    _regions_host = uPP::Array<PrimitiveID>(_regions);
    _regions_dvc = _regions_host.deviceAccess(uPP::transfer::to, _ctx);

    _octreeNodes_host = uPP::Array<Octree>(_octreeNodes);
    _octreeNodes_dvc = _octreeNodes_host.deviceAccess(uPP::transfer::to, _ctx);
  }

  void setOutputBuffer(const OpenglBuffer& openglBuffer) {
    _oglBuffer = openglBuffer;
  }

  void render(const gb::Camera& camera) {
    PixelRGBA* texPtr = (PixelRGBA*) glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

    auto width = _oglBuffer.width();
    auto height = _oglBuffer.height();
    auto numPixels = width*height;

    float xAngleOffset = camera.fov() / (width - 1);
    float yAngleOffset = camera.verticalFOV() / (height - 1);

    {
      auto tex_host = uPP::Array<PixelRGBA>(texPtr, numPixels);
      auto tex_dvc = tex_host.deviceAccess(uPP::transfer::from, _ctx);

      _ctx.forall(uPP::range(numPixels), UPP_LAMBDA(uPP::index l){
        uint x = l % width;
        uint y = l / width;
        uint linearIndex = l;

        float xAngle = -camera.fov() / 2;
        float yAngle = -camera.verticalFOV() / 2;

        xAngle += x * xAngleOffset;
        float xValue = tanf( tt::toRadian(xAngle) );
        yAngle += y * yAngleOffset;
        float yValue = tanf( tt::toRadian(yAngle) );

        Ray ray = {camera.position(), {xValue, yValue, -1}};//todo: nicht nur horizontale richtungen erlauben

        float sumMax = 20000;
        float dataSum = 0;

        traverse(_octreeNodes_dvc, ray, dataSum, sumMax, [&](Octree node) mutable {
          for(RegionSizeT i=0; i<node.regionSize; i++) {
            auto tetraID = _regions_dvc[node.regionStart + i];
            auto tetra = _tetras_dvc[tetraID];
            if(tetra.intersects(ray))
              dataSum += _data_dvc[tetraID];
          }
        });

        float intensity = dataSum / sumMax;
        intensity = intensity > 1 ? 1 : intensity;

        tex_dvc[linearIndex] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
      });
    }

    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
  }

  void split(Octree&                            octree,
             std::vector<PrimitiveID>&          regions,
             std::vector<Octree>&               octreeNodes,
             const std::vector<PrimitiveID>&    regionIDs) {

    auto currentRegionSize = octree.regionSize;
    if(currentRegionSize <= _maxRegionSize) {
      octree.regionStart = regions.size();
      regions.insert(std::end(regions), std::begin(regionIDs), std::end(regionIDs));
      return;
    }

    auto ownID = octreeNodes.size() -1;
    auto mid = octree.aaBox.mid();
    auto octants = octree.aaBox.octants();
    octree.leftChild = octreeNodes.size();//leftChild will be inserted next

    std::vector<std::array<bool, 8>> targets(currentRegionSize, {false});
    identifyTargets(targets, regionIDs, mid);

    auto lastChildID = octree.leftChild;

    for(u_short k=0; k<8; k++) {

      RegionSizeT childRegionSize = 0;
      std::vector<PrimitiveID> childRegionIDs;
      for(RegionSizeT i=0; i<currentRegionSize; i++)
        if(targets[i][k]) {
          childRegionIDs.emplace_back(regionIDs[i]);
          childRegionSize++;
        }

      Octree child;
      child.aaBox = octants[k];
      child.regionSize = childRegionSize;
      child.parent = ownID;

      auto childID = octreeNodes.size();
      octreeNodes.emplace_back(child);
      octreeNodes[lastChildID].rightSibling = childID;
      lastChildID = childID;

      split(octreeNodes[childID], regions, octreeNodes, childRegionIDs);
    }
  }

  void identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                       const std::vector<PrimitiveID>&    regionIDs,
                       Point                              mid) {
    u_long regionSize = regionIDs.size();

    auto targets_host = uPP::Array<std::array<bool, 8>>(targets);
    auto targets_dvc = targets_host.deviceAccess(uPP::transfer::toFrom, _ctx);

    auto regionIDs_host = uPP::Array<PrimitiveID>(regionIDs);
    auto regionIDs_dvc = regionIDs_host.deviceAccess(uPP::transfer::to_const, _ctx);

    _ctx.forall(uPP::range(regionSize), UPP_LAMBDA(uPP::index l) {
      auto tetraID = regionIDs_dvc[l];
      auto tetra = _tetras_dvc[tetraID];
      for(auto const& point : tetra.points()) {
        u_int8_t target = 0;
        for(u_short k=0; k<3; k++) {
          u_int8_t isRight = point[k] > mid[k];
          target += (1u<<(2u-k)) * isRight;
        }
        targets_dvc[l][target] = true;
      }
    });
  }

  uPP::Context& _ctx;
  size_t _nTetras = 0;
  int _maxRegionSize;
  uPP::Array<Tetrahedron> _tetras_host;
  uPP::Array<Tetrahedron>::ConstView _tetras_dvc;
  uPP::Array<fp_t> _data_host;
  uPP::Array<fp_t>::ConstView _data_dvc;
  std::vector<PrimitiveID> _regions;
  uPP::Array<PrimitiveID> _regions_host;
  uPP::Array<PrimitiveID>::View _regions_dvc;
  std::vector<Octree> _octreeNodes;
  uPP::Array<Octree> _octreeNodes_host;
  uPP::Array<Octree>::View _octreeNodes_dvc;

  OpenglBuffer _oglBuffer;
};

OctreeGridSearch::OctreeGridSearch(uPP::Context& ctx)
: _impl(std::make_unique<Impl>(ctx))
{}

OctreeGridSearch::~OctreeGridSearch() = default;

void OctreeGridSearch::construct(const gb::Grid& grid, u_long maxRegionSize) {
  _impl->construct(grid, maxRegionSize);
}

void OctreeGridSearch::setOutputBuffer(const OpenglBuffer& openglBuffer) {
  _impl->setOutputBuffer(openglBuffer);
}

void OctreeGridSearch::render(const gb::Camera& camera) {
  _impl->render(camera);
}
