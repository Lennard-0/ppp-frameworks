## local setup on Ubuntu 20.04
prerequisites:
```bash
sudo apt update
sudo apt install -y software-properties-common apt-transport-https ca-certificates gnupg apt-utils wget curl git gcc-10 g++-10 libilmbase-dev libglew-dev libglfw3-dev
```
The default compiler should be set to gcc, otherwise the Cuda installation will get stuck. Later, the default compiler is set to clang.
```bash
sudo update-alternatives --install /usr/bin/cc cc /usr/bin/gcc-10 100
sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/g++-10 100
```
install CMake:
```bash
wget -qO - https://apt.kitware.com/keys/kitware-archive-latest.asc | sudo apt-key add -
sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ focal main'
sudo apt update
sudo apt install -y cmake
```
install Cuda:
```bash
wget https://developer.download.nvidia.com/compute/cuda/repos/ubuntu2004/x86_64/cuda-ubuntu2004.pin
sudo mv cuda-ubuntu2004.pin /etc/apt/preferences.d/cuda-repository-pin-600
wget https://developer.download.nvidia.com/compute/cuda/11.2.1/local_installers/cuda-repo-ubuntu2004-11-2-local_11.2.1-460.32.03-1_amd64.deb && \
sudo dpkg -i cuda-repo-ubuntu2004-11-2-local_11.2.1-460.32.03-1_amd64.deb
sudo apt-key add /var/cuda-repo-ubuntu2004-11-2-local/7fa2af80.pub
rm cuda-repo-ubuntu2004-11-2-local_11.2.1-460.32.03-1_amd64.deb
sudo apt update && sudo apt-get install -y cuda
```
additional packages for cuda-gdb:
```bash
sudo apt install -y libtinfo5 libncurses5 libncursesw5
```
set the default compiler to clang:
```bash
sudo update-alternatives --install /usr/bin/cc cc /usr/bin/clang-11 110
sudo update-alternatives --install /usr/bin/c++ c++ /usr/bin/clang++-11 110
```
install docker:
```bash
sudo apt install -y docker.io
sudo groupadd -f docker
sudo usermod -aG docker $USER
```

reboot your system

## build & run:
unifiedPP example:
```bash
./buildDocker.sh
./runAll-SaxpyUnifiedPP.py
```
CaptainFuture:
```bash
mkdir -p build
cd build
cmake ..
make -j $(nproc)
./examples/testAsyncNodes/testAsyncNodes
./examples/testPipeline/testPipeline
```
