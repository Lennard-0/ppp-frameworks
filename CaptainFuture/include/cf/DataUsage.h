//
// Created by lennard on 4/30/21.
//

#pragma once

namespace cf {

  struct DataUsage {
    bool input;
    bool output;
  };
  const DataUsage Input = {true, false};
  const DataUsage Output = {false, true};
  const DataUsage InputOutput = {true, true};

}