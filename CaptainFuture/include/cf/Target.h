//
// Created by lennard on 4/30/21.
//

#pragma once

namespace cf {

  enum Target {
    CPU,
    GPU
  };

  Target defaultTarget() const {
    #if CF_DEFAULT_TARGET_GPU
      return Target::GPU;
    #else
      return Target::CPU;
    #endif
  }

  std::vector<Target> getTargets() const {
    return {Target::CPU, Target::GPU};
  }

}