//
// Created by lennard on 5/3/21.
//

#pragma once

#include "cf/Target.h"

namespace cf {

  enum DataLocation {
    Host_external,
    Host_managed,
    CudaGPU
  };

  constexpr bool hasAccess(Target target, DataLocation dataLocation) {
    switch (dataLocation) {
      case DataLocation::Host_external:
        return target == CPU;
      case DataLocation::Host_managed:
        return target == CPU;
      case DataLocation::CudaGPU:
        return target == GPU;
    }
    throw std::runtime_error("unknown DataLocation");
  }

  constexpr DataLocation defaultDataLocation(Target target) {
    switch (target) {
      case CPU:
        return DataLocation::Host_managed;
      case GPU:
        return DataLocation::CudaGPU;
    }
    throw std::runtime_error("unknown Target");
  }

  std::string toString(DataLocation dataLocation) {
    switch (dataLocation) {
      case DataLocation::Host_external:
        return "Host_external";
      case DataLocation::Host_managed:
        return "Host_managed";
      case DataLocation::CudaGPU:
        return "CudaGPU";
    }
    throw std::runtime_error("unknown DataLocation");
  }

}