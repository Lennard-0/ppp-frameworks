//
// Created by lennard on 5/3/21.
//

#pragma once

namespace tt {

  using NodeID = int;

  template<typename T>
  class Node {
  public:
    using Value = T;

    Node(const Value& value, NodeID id)
    : value_{value}
    , id_{id}
    {}

    [[nodiscard]]
    NodeID id() const {
      return id_;
    }

    Value& value() {
      return value_;
    }

    const Value& value() const {
      return value_;
    }

    bool operator<(const Node<T>& other) const {
      return id_ < other.id_;
    }

  private:
    NodeID id_;
    Value value_;
  };

  template<typename T>
  class Edge {
  public:
    using Value = T;

    Edge(const NodeID& sourceId, const NodeID& sinkId, const Value& value)
    : sourceId_{sourceId}
    , sinkId_{sinkId}
    , value_{value}
    {}

    [[nodiscard]]
    NodeID sourceId() const {
      return sourceId_;
    }

    [[nodiscard]]
    NodeID sinkId() const {
      return sinkId_;
    }

    [[nodiscard]]
    Value value() const {
      return value_;
    }

  private:
    NodeID sourceId_;
    NodeID sinkId_;
    Value value_;
  };

  template<typename T, typename E>
  class DAG {
  public:
    using NodeValue = T;
    using NodeType = Node<NodeValue>;
    using EdgeValue = E;
    using EdgeType = Edge<EdgeValue>;

    [[nodiscard]]
    size_t size() const {
      return nodes_.size();
    }

    void addNode(const NodeValue& nodeValue, const std::vector<std::pair<NodeType, EdgeValue>>& predecessorNodes) {
      auto nodeId = idCounter_++;
      nodes_.emplace(nodeId, NodeType{nodeValue, nodeId});
      for(auto const& predecessorNode : predecessorNodes)
        edges_.emplace_back(predecessorNode.first.id(), nodeId, predecessorNode.second);
    }

    std::set<NodeType> nodes() const {
      std::set<NodeType> result;
      for(const NodeType& node : tt::values(nodes_))
        result.insert(node);
      return result;
    }

    std::set<NodeType> predecessor(const NodeType& node) const {
      auto nodeId = node.id();
      std::set<NodeType> result;
      for(auto const& edge : edges_)
        if(edge.sinkId() == nodeId)
          result.insert(nodes_.at(edge.sourceId()));
      return result;
    }

    std::set<NodeType> successor(const NodeType& node) const {
      auto nodeId = node.id();
      std::set<NodeType> result;
      for(auto const& edge : edges_)
        if(edge.sourceId() == nodeId)
          result.insert(nodes_.at(edge.sinkId()));
      return result;
    }

    std::set<NodeType> startingNodes() const {
      std::set<NodeType> result;
      for(NodeType& node : tt::values(nodes_))
        result.insert(node);
      for(auto const& edge : edges_)
        result.erase(nodes_.at(edge.sinkId()));
      return result;
    }

  private:
    NodeID idCounter_ = 0;
    std::map<NodeID, NodeType> nodes_;
    std::vector<EdgeType> edges_;
  };
}