//
// Created by lennard on 4/4/21.
//

#pragma once

#include "DataLocation.h"
#include <atomic>
#include <stdexcept>

namespace cf {

  using ArrayHandlerID = int;
  static std::atomic <ArrayHandlerID> ah_id_counter;

  class ArrayHandlerInterface {
  public:
    [[nodiscard]]
    auto id() const -> ArrayHandlerID {
      return id_;
    }
    virtual void transferTo(DataLocation sink) {
      throw std::runtime_error(
        "running parent implementation of " + std::string(__PRETTY_FUNCTION__) + " instead of specialized one");
    }
    virtual void setCurrentDataLocation(DataLocation current) {
      throw std::runtime_error(
        "running parent implementation of " + std::string(__PRETTY_FUNCTION__) + " instead of specialized one");
    }
  private:
    ArrayHandlerID id_ = ah_id_counter++;
  };

}