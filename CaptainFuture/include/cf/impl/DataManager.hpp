//
// Created by lennard on 5/3/21.
//

#pragma once

#include <cstring>
#include <memory>

namespace cf {

  template<typename T>
  class DataManager {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    DataManager(size_t size, DataLocation dataLocation)
      : size_{size}, dataLocation_{dataLocation} {}

    size_t size() const {
      return size_;
    }

    virtual ConstView constView() = 0;

    virtual View view() = 0;

    DataLocation dataLocation() const {
      return dataLocation_;
    }

  private:
    size_t size_;
    DataLocation dataLocation_;
  };

  template<typename T>
  class HostData : public DataManager<T> {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    HostData(ConstView constView, size_t size)
      : DataManager<T>(size, DataLocation::Host_external), constView_{constView} {}

    HostData(View view, size_t size)
      : DataManager<T>(size, DataLocation::Host_external), constView_{view}, view_{view} {}

    ConstView constView() override {
      return constView_;
    }

    View view() override {
      if (view_ == nullptr)
        throw std::runtime_error("mutable view pointer HostData::view_ == nullptr");
      return view_;
    }

  private:
    View view_ = nullptr;
    ConstView constView_ = nullptr;
  };

  template<typename T>
  class ManagedData : public DataManager<T> {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    ManagedData(size_t size)
    : DataManager<T>(size, DataLocation::Host_managed)
    , data_((T*)std::malloc(size*sizeof(T)))
    {}

    ~ManagedData()
    {
      std::free(data_);
    }

    ConstView constView() override {
      return data_;
    }

    View view() override {
      return data_;
    }

  private:
    T* data_;
  };

  template<typename T>
  class CudaData : public DataManager<T> {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    CudaData(size_t size)
      : DataManager<T>(size, DataLocation::CudaGPU) {
      view_ = cudaAllocate<Value>(DataManager<T>::size());
      constView_ = view_;
    }

    ~CudaData() {
      cudaDeviceFree(view_);
    }

    ConstView constView() override {
      return constView_;
    }

    View view() override {
      return view_;
    }

  private:
    View view_ = nullptr;
    ConstView constView_ = nullptr;
  };

  template<typename T>
  void transfer(DataManager<T>& sink, DataManager<T>& source) {
    if (sink.dataLocation() == DataLocation::Host_external && source.dataLocation() == DataLocation::CudaGPU)
      cudaCopyToHost(sink.view(), source.constView(),
                     source.size());//todo: alternative Funktionen statt view/constView nutzen?
    else if (sink.dataLocation() == DataLocation::Host_external && source.dataLocation() == DataLocation::Host_managed)
      std::memcpy(sink.view(), source.constView(), source.size());

    else if (sink.dataLocation() == DataLocation::CudaGPU && source.dataLocation() == DataLocation::Host_external)
      cudaCopyToDevice(sink.view(), source.constView(), source.size());
    else if (sink.dataLocation() == DataLocation::CudaGPU && source.dataLocation() == DataLocation::Host_managed)
      cudaCopyToDevice(sink.view(), source.constView(), source.size());

    else if (sink.dataLocation() == DataLocation::Host_managed && source.dataLocation() == DataLocation::CudaGPU)
      cudaCopyToHost(sink.view(), source.constView(), source.size());
    else if (sink.dataLocation() == DataLocation::Host_managed && source.dataLocation() == DataLocation::Host_external)
      std::memcpy(sink.view(), source.constView(), source.size());
  }

}