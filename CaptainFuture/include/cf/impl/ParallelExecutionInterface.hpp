//
// Created by lennard on 4/4/21.
//

#pragma once

namespace cf {

  class ParallelExecutionInterface {
  public:
    virtual void runCuda() {
      throw std::runtime_error(
        "running parent implementation of " + std::string(__PRETTY_FUNCTION__) + " instead of specialized one");
    }
    virtual void runOpenMP() {
      throw std::runtime_error(
        "running parent implementation of " + std::string(__PRETTY_FUNCTION__) + " instead of specialized one");
    }
  };
}