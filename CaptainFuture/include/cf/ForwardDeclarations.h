//
// Created by lennard on 5/3/21.
//

#pragma once

#include <chrono>

#define CF_LAMBDA [=] __host__ __device__

#define throwForbiddenParentFunctionException() throw std::runtime_error("running parent implementation of " + std::string(__PRETTY_FUNCTION__) + " instead of specialized one")

namespace cf {

  class Node;

  std::chrono::nanoseconds elapsedTime() {
    static auto t0 = std::chrono::steady_clock::now();
    return std::chrono::steady_clock::now() - t0;
  }
}