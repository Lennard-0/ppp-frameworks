//
// Created by lennard on 4/30/21.
//

#pragma once

#include "ArrayHandler.hpp"
#include "Node.hpp"
#include "impl/DAG.hpp"
#include "join.hpp"
#include <tt/prettyStream.h>

namespace cf {

  class Pipeline {
  public:
    using DAGType = tt::DAG<Node, ArrayHandlerID>;
    using DAGNode = typename DAGType::NodeType;

    template<typename F>
    void addNode(F f, Target target) {

      Node node{target};
      f(node);

      std::vector<std::pair<DAGNode, ArrayHandlerID>> predecessorNodes;
      for(const DAGNode& dagNode : dag_.nodes())//todo
        for(auto writeId : dagNode.value().writeDependencies())
          for(auto readId : node.readDependencies())
            if(writeId == readId)
              predecessorNodes.emplace_back(dagNode, writeId);

      dag_.addNode(node, predecessorNodes);

    }

    [[nodiscard]] bool canRun(const DAGNode& node, const std::set<DAGNode>& executedNodes) const {
      bool result = ! executedNodes.count(node);//todo: use contains (C++20)
      for(auto const& predecessor : dag_.predecessor(node))
        if(!executedNodes.count(predecessor)) //todo: use contains (C++20)
          result = false;
      return result;
    }

    void run() {

      auto numNodes = dag_.size();
      std::cout << numNodes << " nodes added\n";

      auto startingNodes = dag_.startingNodes();
      if(startingNodes.empty())
        throw std::runtime_error("no starting node found");

      std::cout << "IDs of starting nodes:";
      for(auto const& startingNode : startingNodes)
        std::cout << " " << startingNode.id();
      std::cout << '\n';

      std::set<DAGNode> executedNodes;

      std::function<void(DAGNode&)> executeNode = [&] (DAGNode& node) {
        std::cout << "executing node " << node.id() << '\n';
        auto t0 = std::chrono::steady_clock::now();
        node.value().execute();
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "node " << node.id() << " execution time: " << t1-t0 << '\n';
        executedNodes.insert(node);
        for(auto successorNode : dag_.successor(node))
          if(canRun(successorNode, executedNodes))
            executeNode(successorNode);
      };

      for(auto startingNode : startingNodes)
        executeNode(startingNode);

    }

  private:
    DAGType dag_;
  };

}