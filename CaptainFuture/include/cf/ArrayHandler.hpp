//
// Created by lennard on 4/30/21.
//

#pragma once

#include "ForwardDeclarations.h"
#include "Target.h"
#include "impl/CudaFunctions.cu"
#include "impl/ArrayHandlerInterface.hpp"
#include "DataUsage.h"
#include "impl/DataManager.hpp"
#include "MemorySpace.h"
#include <tt/container-functions.hpp>
#include <tt/prettyStream.h>
#include <exception>
#include <map>
#include <set>
#include <memory>

namespace cf {

  template<typename T>
  class ArrayHandler : public ArrayHandlerInterface {
  public:
    using Value = T;
    using View = T*;
    using ConstView = const T*;

    ArrayHandler(const T* dataPtr, size_t size, DataUsage usage=Input)
    : size_{size}
    {
      if (usage.output)
        throw std::runtime_error("cant use const vector as output");
      dataLocations_.emplace(DataLocation::Host_external, std::make_unique<HostData<T>>(dataPtr, size_));
      currentDataLocations_.emplace(DataLocation::Host_external);
    }
    ArrayHandler(T* dataPtr, size_t size, DataUsage usage=InputOutput)
    : size_{size}
    {
      dataLocations_.emplace(DataLocation::Host_external, std::make_unique<HostData<T>>(dataPtr, size_));
      if(usage.input)
        currentDataLocations_.emplace(DataLocation::Host_external);
      if(usage.output)
        copyBack_ = true;
    }
    ArrayHandler(const std::vector<T>& data, DataUsage usage=Input)
    : size_{data.size()}
    {
      if (usage.output)
        throw std::runtime_error("cant use const vector as output");
      dataLocations_.emplace(DataLocation::Host_external, std::make_unique<HostData<T>>(data.data(), size_));
      currentDataLocations_.emplace(DataLocation::Host_external);
    }
    ArrayHandler(std::vector<T>& data, DataUsage usage=InputOutput)
    : size_{data.size()}
    {
      dataLocations_.emplace(DataLocation::Host_external, std::make_unique<HostData<T>>(data.data(), size_));
      if(usage.input)
        currentDataLocations_.emplace(DataLocation::Host_external);
      if(usage.output)
        copyBack_ = true;
    }
    ArrayHandler(size_t size)
    : size_{size}
    {}

    ~ArrayHandler() {
      if(copyBack_)
        transferTo(DataLocation::Host_external);
    }

    [[nodiscard]]
    size_t size() const {
      return size_;
    }

    ConstView readView(MemorySpace memorySpace) {
      auto dataLocation = transferTo(memorySpace);
      return dataLocations_.at(dataLocation)->constView();
    }

    View writeView(MemorySpace memorySpace) {
      auto dataLocation = transferTo(memorySpace);
      currentDataLocations_ = {dataLocation};
      return dataLocations_.at(dataLocation)->view();
    }

    View readWriteView(MemorySpace memorySpace) {
      auto dataLocation = transferTo(memorySpace);
      currentDataLocations_ = {dataLocation};
      return dataLocations_.at(dataLocation)->view();
    }

    ConstView readView(Target target = defaultTarget()) {
      return readView(defaultMemorySpace(target));
    }

    View writeView(Target target = defaultTarget()) {
      return writeView(defaultMemorySpace(target));
    }

    View readWriteView(Target target = defaultTarget()) {
      return readWriteView(defaultMemorySpace(target));
    }

    ConstView readView(Node& node) {
      auto targetLocation = ensureExistance(node.target());
      node.addReadDependency(*this, targetLocation);
      return dataLocations_.at(targetLocation)->constView();
    }

    View writeView(Node& node) {
      auto targetLocation = ensureExistance(node.target());
      node.addWriteDependency(*this, targetLocation);
      return dataLocations_.at(targetLocation)->view();
    }

    View readWriteView(Node& node) {
      auto targetLocation = ensureExistance(node.target());
      node.addReadWriteDependency(*this, targetLocation);
      return dataLocations_.at(targetLocation)->view();
    }

    DataLocation transferTo(Target target) {
      auto targetLocation = selectBestDataLocation(target);
      transferTo(targetLocation);
      return targetLocation;
    }

    void transferTo(MemorySpace memorySpace) override {
      if(currentDataLocations_.count(memorySpace))
        return;

      ensureExistance(memorySpace);

      if(!currentDataLocations_.empty()) {
        auto& source = *dataLocations_.at(*currentDataLocations_.begin());
        auto& sink = *dataLocations_.at(targetLocation);
        auto t0 = std::chrono::steady_clock::now();
        transfer(sink, source);
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "AH " << id() <<" transfering data from " << toString(source.dataLocation())
                  << " to " << toString(sink.dataLocation()) << " took " << t1-t0 << " >" << elapsedTime() << '\n';
      }

      currentDataLocations_.emplace(memorySpace);
    }

    void setCurrentDataLocation(DataLocation current) override {
      if(!dataLocations_.count(current))
        throw std::runtime_error("cant set current data location to a location that is not present");
      currentDataLocations_ = {current};
    }

  private:
    DataLocation selectBestDataLocation(Target target) const {
      //todo: make sure the result is writable if needed
      for(auto const& currentLocation : currentDataLocations_)
        if(hasAccess(target, currentLocation))
          return currentLocation;

      auto targetLocation = defaultDataLocation(target);
      if(!dataLocations_.count(targetLocation))//todo: use .contains
        for(auto const& dataLocation : tt::keys(dataLocations_))
          if(hasAccess(target, dataLocation))
            return dataLocation;
      return targetLocation;
    }

    DataLocation ensureExistance(Target target) {
      for(auto const& dataLocation : tt::keys(dataLocations_))
        if(hasAccess(target, dataLocation))
          return dataLocation;

      auto targetLocation = defaultDataLocation(target);
      ensureExistance(targetLocation);
      return targetLocation;
    }

    void ensureExistance(DataLocation targetLocation) {
      if(!dataLocations_.count(targetLocation))
        allocate(targetLocation);
    }

    void allocate(DataLocation targetLocation) {
      auto t0 = std::chrono::steady_clock::now();
      switch (targetLocation) {
        case DataLocation::Host_external:
          throw std::runtime_error("cant allocate external data");
        case DataLocation::Host_managed: {
          dataLocations_.emplace(DataLocation::Host_managed, std::make_unique<ManagedData<T>>(size_));
          break;
        }
        case DataLocation::CudaGPU: {
          dataLocations_.emplace(DataLocation::CudaGPU, std::make_unique<CudaData<T>>(size_));
          break;
        }
      }
      auto t1 = std::chrono::steady_clock::now();
      std::cout << "AH " << id() << ": allocating on " << toString(targetLocation) << " took " << t1-t0 << " >" << elapsedTime() << '\n';
    }

  private:
    size_t size_;
    std::map<DataLocation, std::unique_ptr<DataManager<T>>> dataLocations_;
    std::set<DataLocation> currentDataLocations_;
    bool copyBack_ = false;
  };

}