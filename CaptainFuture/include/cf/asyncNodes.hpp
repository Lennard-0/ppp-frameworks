//
// Created by lennard on 4/30/21.
//

#pragma once

#include "ArrayHandler.hpp"
#include "forall.hpp"
#include "join.hpp"
#include "impl/ParallelForall.hpp"
#include "impl/ParallelReduction.hpp"
#include "impl/ParallelScan.hpp"
#include <future>
#include <mutex>

namespace cf {

  using NodeWrapperID = int;
  static std::atomic <NodeWrapperID> nw_id_counter = 1;
  std::mutex cudaMutex;
  std::mutex ompMutex;

  template<typename F>
  class NodeWrapper {
  public:

    NodeWrapper(F f)
    : f_{f}
    {}

    std::shared_future<void> operator()(const Target& target, std::initializer_list<std::shared_future<void>> predecessor) {
      return std::async(std::launch::async, [=]()
      {
        for(auto & preNode_ftr : predecessor)
          preNode_ftr.wait();
        if(target==cf::GPU)
          cudaMutex.lock();
        else
          ompMutex.lock();
        std::cout << "executing node " << id_ << " in thread " << std::this_thread::get_id() << " >" << elapsedTime() << '\n';
        auto t0 = std::chrono::steady_clock::now();
        f_(target);
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "node " << id_ << " execution time: " << t1-t0 << " >" << elapsedTime() << std::endl;
        if(target==cf::GPU)
          cudaMutex.unlock();
        else
          ompMutex.unlock();
      }).share();
    }

    std::shared_future<void> operator()(const Target& target) {
      return operator()(target, {});
    }

  private:
    F f_;
    NodeWrapperID id_ = nw_id_counter++;
  };

  template<typename F>
  NodeWrapper<F> createNode(F f) {
    return {f};
  }

  template<typename T, typename IF, typename JF>
  void reduce(T& reductionVariable, IF inputFunction, JF joinFunction, size_t N, Target target) {
    std::cout << "start of reduce >" << cf::elapsedTime() << '\n';
    ParallelReduction<T,IF,JF> executor(reductionVariable, inputFunction, joinFunction, N);

    if (target == GPU)
      executor.runCuda();
    else if (target == CPU)
      executor.runOpenMP();
    else
      throw std::runtime_error("unsupported target ");
  }

  template<typename T, typename JF>
  void scan(ArrayHandler<T>& out_ah, ArrayHandler<T>& in_ah, Target target, T neutralElement, JF joinFunction) {
    auto in_dvc = in_ah.readView(target);
    auto inputFunction = CF_LAMBDA(size_t i)->T { return in_dvc[i]; };
    scan(out_ah, inputFunction, target, neutralElement, joinFunction);
  }

  template<typename T, typename IF, typename JF>
  void scan(ArrayHandler<T>& out_ah, IF inputFunction, Target target, T neutralElement, JF joinFunction) {
    auto out_dvc = out_ah.readWriteView(target);
    ParallelScan<T,IF,JF> executor(out_dvc, inputFunction, neutralElement, joinFunction, out_ah.size());

    if (target == GPU)
      executor.runCuda();
    else if (target == CPU)
      executor.runOpenMP();
    else
      throw std::runtime_error("unsupported target ");
  }
}