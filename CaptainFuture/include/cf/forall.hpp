//
// Created by lennard on 4/30/21.
//

#pragma once

#include "ArrayHandler.hpp"
#include "range.hpp"
#include <tt/prettyStream.h>

namespace cf {

  template<typename F>
  void forall(range idxRange, const Target& target, F f) {
    auto N = idxRange.size();
//    auto t0 = std::chrono::steady_clock::now();
    if (target == GPU){
      size_t blockSize = 256;
      size_t numBlocks = (N + blockSize - 1) / blockSize; // aufrunden falls N kein Mehrfaches von blockSize ist

      cudaForallKernel<<<numBlocks, blockSize>>>(f, N);
      checkError(cudaPeekAtLastError());
    }
    else if (target == CPU) {
      #pragma omp parallel for
      for (size_t i = 0; i < N; i++)
        f(i);
    }
    else
      throw std::runtime_error("unsupported target");
//    auto t1 = std::chrono::steady_clock::now();
//    std::cout << "execution of forall took " << t1-t0 << '\n';
  }

  template<typename F>
  void forall(size_t N, F f) {
    forall(N, defaultTarget, f);
  }

}