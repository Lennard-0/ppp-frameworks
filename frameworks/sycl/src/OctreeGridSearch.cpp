//
// Created by lennard on 11/11/20.
//

#include "OctreeGridSearch.h"

#include <algorithms/Octree.h>
#include "../../../libs/scene/include/scene/Camera.h"
#include <image/PixelRGBA.h>
#include <SYCL/sycl.hpp>

class SplitKernel;
class RenderKernel;

namespace gridbox {

  struct OctreeGridSearch::Impl {
    size_t nTetras = 0;
    uint maxRegionSize;
    sycl::buffer<Tetrahedron,1> tetras_buf;
    sycl::buffer<float,1> data_buf;
    sycl::buffer<Octree,1> octreeNodes_buf;
    sycl::buffer<PrimitiveID,1> regions_buf;
    OpenglBuffer oglBuffer;

    void split(Octree&                            octree,
               std::vector<PrimitiveID>&          regions,
               std::vector<Octree>&               octreeNodes,
               const std::vector<PrimitiveID>&    regionIDs);

    void identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                         const std::vector<PrimitiveID>&    regionIDs,
                         Point                              mid);
  };


  OctreeGridSearch::OctreeGridSearch()
  : _impl(std::make_unique<Impl>())
  {}

//OctreeGridSearch::OctreeGridSearch(const OctreeGridSearch& other)
//: _impl(std::make_unique<Impl>(*other._impl))
//{}
//
//OctreeGridSearch::OctreeGridSearch(OctreeGridSearch&& other) = default;
//
//OctreeGridSearch& OctreeGridSearch::operator=(const OctreeGridSearch &other)
//{
//  *_impl = *other._impl;
//  return *this;
//}
//
//OctreeGridSearch& OctreeGridSearch::operator=(OctreeGridSearch&&) = default;

  OctreeGridSearch::~OctreeGridSearch() = default;


  void OctreeGridSearch::construct(const gridbox::Grid& grid, u_long maxRegionSize) {
    LOG(INFO) << "running with: SYCL";
    std::cout << "known platforms:\n";
    for(auto const& platform : sycl::platform::get_platforms())
      std::cout << "  " << platform.get_info<sycl::info::platform::name>() << '\n';
    std::cout << "known devices:\n";
    for(auto const& device : sycl::device::get_devices()) {
      std::string typeStr = device.is_gpu() ? "gpu" : "no gpu";
      std::cout << "  " << typeStr << '\n';
    }

    _impl->nTetras = grid.size();
    _impl->maxRegionSize = maxRegionSize;

    _impl->tetras_buf = sycl::buffer<Tetrahedron, 1>(grid.tetrahedronsPtr(), sizeof(Tetrahedron) * _impl->nTetras);
    _impl->data_buf = sycl::buffer<float, 1>(grid.dataPtr(), sizeof(float) * _impl->nTetras);

    std::vector<Octree> octreeNodes;

    std::vector<PrimitiveID> ids(_impl->nTetras);
    std::iota(ids.begin(), ids.end(), 0);

    Octree root;
    root.aaBox = grid.aabb();
    root.regionSize = _impl->nTetras;
    octreeNodes.emplace_back(root);

    std::vector<PrimitiveID> regions;
    regions.reserve(_impl->nTetras*3);

    _impl->split(octreeNodes[0], regions, octreeNodes, ids);

    regions.shrink_to_fit();
    auto totalLeafTetras = regions.size();
    _impl->regions_buf = sycl::buffer<PrimitiveID,1>(regions.data(), totalLeafTetras);

    LOG(INFO) << "sum of all region sizes: " << totalLeafTetras
              << " -> x" << (double)totalLeafTetras/_impl->nTetras << " increase\n";
    LOG(INFO) << "total tree nodes: " << octreeNodes.size();

    _impl->octreeNodes_buf = sycl::buffer<Octree,1>(octreeNodes.data(), octreeNodes.size());
  }

  void OctreeGridSearch::Impl::identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                                               const std::vector<PrimitiveID>&    regionIDs,
                                               Point                              mid) {
    u_long regionSize = regionIDs.size();

    {
      sycl::buffer<std::array<bool, 8>,1> targets_buf(targets.data(), regionSize);
      sycl::buffer<PrimitiveID,1> regionIDs_buf(regionIDs.data(), regionSize);

      sycl::queue q;
      q.submit([&](sycl::handler& cgh) {

        auto targets_acc = targets_buf.get_access<sycl::access::mode::write>(cgh);
        auto tetras_acc = tetras_buf.get_access<sycl::access::mode::read>(cgh);
        auto regionIDs_acc = regionIDs_buf.get_access<sycl::access::mode::read>(cgh);

        cgh.parallel_for<SplitKernel>(sycl::range<1>(regionSize), [=](sycl::id<1> idx){
          auto tetraID = regionIDs_acc[idx];
          auto tetra = tetras_acc[tetraID];
          for(auto const& point : tetra.points()) {
            u_int8_t target = 0;
            for(u_short k=0; k<3; k++) {
              u_int8_t isRight = point[k] > mid[k];
              target += (1u<<(2u-k)) * isRight;
            }
            targets_acc[idx][target] = true;
          }
        });
      });
    }
  }


  void OctreeGridSearch::Impl::split(Octree&                          octree,
                                     std::vector<PrimitiveID>&        regions,
                                     std::vector<Octree>&             octreeNodes,
                                     const std::vector<PrimitiveID>&  regionIDs) {

    auto currentRegionSize = octree.regionSize;
    if(currentRegionSize <= maxRegionSize) {
      octree.regionStart = regions.size();
      regions.insert(std::end(regions), std::begin(regionIDs), std::end(regionIDs));
      return;
    }

    auto ownID = octreeNodes.size() -1;
    auto mid = octree.aaBox.mid();
    auto octants = octree.aaBox.octants();
    octree.leftChild = octreeNodes.size();//leftChild will be inserted next

    std::vector<std::array<bool, 8>> targets(currentRegionSize, {false});
    identifyTargets(targets, regionIDs, mid);

    auto lastChildID = octree.leftChild;

    for(u_short k=0; k<8; k++) {

      RegionSizeT childRegionSize = 0;
      std::vector<PrimitiveID> childRegionIDs;
      for(RegionSizeT i=0; i<currentRegionSize; i++)
        if(targets[i][k]) {
          childRegionIDs.emplace_back(regionIDs[i]);
          childRegionSize++;
        }

      Octree child;
      child.aaBox = octants[k];
      child.regionSize = childRegionSize;
      child.parent = ownID;

      auto childID = octreeNodes.size();
      octreeNodes.emplace_back(child);
      octreeNodes[lastChildID].rightSibling = childID;
      lastChildID = childID;

      split(octreeNodes[childID], regions, octreeNodes, childRegionIDs);
    }
  }

  void OctreeGridSearch::setOutputBuffer(const OpenglBuffer& openglBuffer) {
    _impl->oglBuffer = openglBuffer;
  }

  void OctreeGridSearch::render(const Camera& camera) {

    PixelRGBA* texPtr = (PixelRGBA*) glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

    auto width = _impl->oglBuffer.width();
    auto height = _impl->oglBuffer.height();
    auto numPixels = width*height;

    float xAngleOffset = camera.fov() / (width - 1);
    float yAngleOffset = camera.verticalFOV() / (height - 1);


    {
      sycl::buffer<PixelRGBA,1> tex_buf(texPtr, numPixels);

      sycl::queue q;
      q.submit([&](sycl::handler& cgh) {

        auto tex_acc = tex_buf.get_access<sycl::access::mode::discard_write>(cgh);
        auto tetras_acc = _impl->tetras_buf.get_access<sycl::access::mode::read>(cgh);
        auto data_acc = _impl->data_buf.get_access<sycl::access::mode::read>(cgh);
        auto regions_acc = _impl->regions_buf.get_access<sycl::access::mode::read>(cgh);
        auto octreeNodes_acc = _impl->octreeNodes_buf.get_access<sycl::access::mode::read>(cgh);

        cgh.parallel_for<RenderKernel>(sycl::range<2>(width, height), [=](sycl::id<2> idx){
          uint x = idx.get(0);
          uint y = idx.get(1);
          uint linearIndex = y * width + x;

          float xAngle = -camera.fov() / 2;
          float yAngle = -camera.verticalFOV() / 2;

          xAngle += x * xAngleOffset;
          float xValue = tanf( toRadian(xAngle) );
          yAngle += y * yAngleOffset;
          float yValue = tanf( toRadian(yAngle) );

          Ray ray = {camera.position(), {xValue, yValue, -1}};//todo: nicht nur horizontale richtungen erlauben

          float sumMax = 20000;
          float dataSum = 0;

          traverse(octreeNodes_acc.get_pointer(), ray, dataSum, sumMax, [&](Octree node) mutable {
            for(RegionSizeT i=0; i<node.regionSize; i++) {
              auto tetraID = regions_acc[node.regionStart + i];
              auto tetra = tetras_acc[tetraID];
              if(tetra.intersects(ray))
                dataSum += data_acc[tetraID];
            }
          });

          float intensity = dataSum / sumMax;
          intensity = intensity > 1 ? 1 : intensity;

          tex_acc[linearIndex] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
        });

      });
    }

    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);

  }
}