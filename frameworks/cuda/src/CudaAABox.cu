//
// Created by lennard on 10/28/20.
//

#include "CudaAABox.h"

CudaPoint CudaAABox::mid() const {
  return min.halfWayTo(max);
}

__host__ __device__ bool CudaAABox::containsInInterior(const CudaPoint point) const {
//  for(u_short d=0; d<3; d++)
//    if( min[d] >= point[d]  ||  max[d] <= point[d] )
//      return false;
//
//  return true;
  return min[0] < point[0] && max[0] > point[0] &&
         min[1] < point[1] && max[1] > point[1] &&
         min[2] < point[2] && max[2] > point[2];
}
