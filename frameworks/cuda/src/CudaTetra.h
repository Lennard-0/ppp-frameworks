//
// Created by lennard on 10/30/20.
//

#pragma once

#include "CudaTriangle.h"

class CudaTetra {
public:
  CudaPoint p0, p1, p2, p3;
  __host__ __device__
  CudaPoint point(const u_short & i) const {
    switch (i) {
      case 0:
        return p0;
      case 1:
        return p1;
      case 2:
        return p2;
      case 3:
        return p3;
      default:
//        throw std::out_of_range(std::to_string(i) + " is not in range [0,3]");
      {errno = 4711;
        return {};}
    }
  }

//  __host__ __device__
//  void getTriangles(CudaTriangle (&triangles)[4]) const {
//    triangles[0] = {p1,p2,p3};
//    triangles[1] = {p0,p3,p2};
//    triangles[2] = {p0,p1,p3};
//    triangles[3] = {p0,p2,p1};
//  }
  __host__ __device__
  void getTriangles(CudaTriangle* triangles) const {
    triangles[0] = {p1,p2,p3};
    triangles[1] = {p0,p3,p2};
    triangles[2] = {p0,p1,p3};
    triangles[3] = {p0,p2,p1};
  }

//  __host__ __device__
//  CudaTriangle[4] triangles() const {
//
//  }
};