//
// Created by lennard on 10/28/20.
//

#include "CudaVector.h"

//inline bool operator==(const CudaVector& lhs, const CudaVector& rhs);
//bool operator<(const CudaVector& lhs, const CudaVector& rhs);
//bool operator>(const CudaVector& lhs, const CudaVector& rhs);
//
//CudaVector operator+(const CudaVector& lhs, const CudaVector& rhs);
CudaVector operator-(const CudaVector lhs, const CudaVector rhs) {
  return {lhs.x()-rhs.x(), lhs.y()-rhs.y(), lhs.z()-rhs.z()};
}
CudaVector operator*(const float scalar, const CudaVector vector) {
  return {scalar*vector.x(), scalar*vector.y(), scalar*vector.z()};
}
//CudaVector operator*(const CudaVector& vector, const float& scalar);
//CudaVector operator/(const CudaVector& vector, const float& scalar);
__host__ __device__ float CudaVector::dot(const CudaVector other) const {
  return x()*other.x() + y()*other.y() + z()*other.z();
}

__host__ __device__ CudaVector CudaVector::cross(const CudaVector other) const {
  return {
    y() * other.z() - z() * other.y(),
    z() * other.x() - x() * other.z(),
    x() * other.y() - y() * other.x()
  };
}
