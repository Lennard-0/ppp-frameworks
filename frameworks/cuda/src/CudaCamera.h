//
// Created by lennard on 11/1/20.
//

#pragma once

#include "CudaPoint.h"
#include "../../../libs/scene/include/scene/Camera.h"


class CudaCamera {
public:
  explicit CudaCamera(const gridbox::Camera& camera);

  CudaPoint position ;
  CudaVector direction;
  double fov = 70;
  double verticalFOV = 70;
  double aspectRatio = 1;
};