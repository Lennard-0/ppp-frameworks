//
// Created by lennard on 11/1/20.
//

#include "CudaCamera.h"

CudaCamera::CudaCamera(const gridbox::Camera& camera)
: position(camera.position())
, direction(camera.direction())
, fov(camera.fov())
, verticalFOV(camera.verticalFOV())
, aspectRatio(camera.aspectRatio())
{}
