//
// Created by lennard on 11/11/20.
//

#include "OctreeGridSearch.h"

#include <algorithms/Octree.h>
#include "../../../libs/scene/include/scene/Camera.h"
#include <image/PixelRGBA.h>
#define __CL_ENABLE_EXCEPTIONS
//#define CL_ENABLE_EXCEPTIONS
//#define CL_HPP_ENABLE_EXCEPTIONS
#define CL_HPP_TARGET_OPENCL_VERSION 120
#define CL_HPP_MINIMUM_OPENCL_VERSION 120
#include <CL/cl2.hpp>
#include <assert.h>

class SplitKernel;
class RenderKernel;

namespace gridbox {

  struct OctreeGridSearch::Impl {
    size_t nTetras = 0;
    uint maxRegionSize;
    cl::Device clDevice;
    cl::Context clContext;
    cl::Program clIdentifyTargetsProgram;
    cl::Program clRenderProgram;
    cl::Buffer tetras_buf;
    cl::Buffer data_buf;
    cl::Buffer octreeNodes_buf;
    cl::Buffer regions_buf;
    OpenglBuffer oglBuffer;

    void split(Octree&                            octree,
               std::vector<PrimitiveID>&          regions,
               std::vector<Octree>&               octreeNodes,
               const std::vector<PrimitiveID>&    regionIDs);

    void identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                         const std::vector<PrimitiveID>&    regionIDs,
                         Point                              mid);
  };


  OctreeGridSearch::OctreeGridSearch()
  : _impl(std::make_unique<Impl>())
  {
    std::vector<cl::Platform> platforms;
    cl::Platform::get(&platforms);
    assert(!platforms.empty());

    LOG(INFO) << "running with: OpenCL";
    LOG(INFO) << "known platforms:";
    for(auto const& platform : platforms) {
      LOG(INFO) << "  " << platform.getInfo<CL_PLATFORM_NAME>() << " with OpenCL version: " << platform.getInfo<CL_PLATFORM_VERSION>();
      std::vector<cl::Device> devices;
      platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
      assert(!devices.empty());

      LOG(INFO) << "    with devices:";
      for(auto const& device : devices)
        LOG(INFO) << "      " << device.getInfo<CL_DEVICE_NAME>();
    }
    auto platform = platforms.front();
    std::vector<cl::Device> devices;
    platform.getDevices(CL_DEVICE_TYPE_ALL, &devices);
    _impl->clDevice = devices.front();
    LOG(INFO) << "using device " << _impl->clDevice.getInfo<CL_DEVICE_NAME>()
              << " from platform " << platform.getInfo<CL_PLATFORM_NAME>();

    _impl->clContext = cl::Context(_impl->clDevice);
  }

//OctreeGridSearch::OctreeGridSearch(const OctreeGridSearch& other)
//: _impl(std::make_unique<Impl>(*other._impl))
//{}
//
//OctreeGridSearch::OctreeGridSearch(OctreeGridSearch&& other) = default;
//
//OctreeGridSearch& OctreeGridSearch::operator=(const OctreeGridSearch &other)
//{
//  *_impl = *other._impl;
//  return *this;
//}
//
//OctreeGridSearch& OctreeGridSearch::operator=(OctreeGridSearch&&) = default;

  OctreeGridSearch::~OctreeGridSearch() = default;

  cl::Program createProgram(const cl::Context& context, const std::filesystem::path& kernelFilePath) {

    cl_int err = 0;
    cl::Program program(context, readFileToString(kernelFilePath), false, &err);
    if(err)
      throw std::runtime_error("OpenCL error: " + std::to_string(err) + " in " + __PRETTY_FUNCTION__ );

    try
    {
      program.build("-cl-std=CL1.2");
    }
    catch (cl::Error& e)
    {
      if (e.err() == CL_BUILD_PROGRAM_FAILURE) {
        for (cl::Device dev : program.getInfo<CL_PROGRAM_DEVICES>()) {
          // Check the build status
          cl_build_status status = program.getBuildInfo<CL_PROGRAM_BUILD_STATUS>(dev);
          if (status != CL_BUILD_ERROR)
            continue;

          // Get the build log
          std::string name = dev.getInfo<CL_DEVICE_NAME>();
          std::string buildlog = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(dev);
          std::cerr << "Build log for " << name << ":" << std::endl
                    << buildlog << std::endl;
        }
        throw e;
      }
      else
      {
        throw e;
      }
    }
//    throw std::runtime_error("OpenCL error: " + std::to_string(err) + " in " + __PRETTY_FUNCTION__ );

    return program;
  }

  void OctreeGridSearch::construct(const gridbox::Grid& grid, u_long maxRegionSize) {
    _impl->clIdentifyTargetsProgram = createProgram(_impl->clContext, "opencl/src/IdentifyTargetsKernel.cl");
    _impl->clRenderProgram = createProgram(_impl->clContext, "opencl/src/RenderKernel.cl");

    _impl->nTetras = grid.size();
    _impl->maxRegionSize = maxRegionSize;

    _impl->tetras_buf = cl::Buffer(_impl->clContext,
                                   CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
                                   sizeof(Tetrahedron) * _impl->nTetras,
                                   (void*) grid.tetrahedronsPtr());
    _impl->data_buf = cl::Buffer(_impl->clContext,
                                 CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
                                 sizeof(float) * _impl->nTetras,
                                 (void*) grid.dataPtr());

    std::vector<Octree> octreeNodes;

    std::vector<PrimitiveID> ids(_impl->nTetras);
    std::iota(ids.begin(), ids.end(), 0);

    Octree root;
    root.aaBox = grid.aabb();
    root.regionSize = _impl->nTetras;
    octreeNodes.emplace_back(root);

    std::vector<PrimitiveID> regions;
    regions.reserve(_impl->nTetras*3);

    _impl->split(octreeNodes[0], regions, octreeNodes, ids);

    regions.shrink_to_fit();
    auto totalLeafTetras = regions.size();
    _impl->regions_buf = cl::Buffer(_impl->clContext,
                                    CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
                                    sizeof(PrimitiveID) * totalLeafTetras,
                                    (void*) regions.data());

    LOG(INFO) << "sum of all region sizes: " << totalLeafTetras
              << " -> x" << (double)totalLeafTetras/_impl->nTetras << " increase\n";
    LOG(INFO) << "total tree nodes: " << octreeNodes.size();

    _impl->octreeNodes_buf = cl::Buffer(_impl->clContext,
                                        CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
                                        sizeof(Octree) * octreeNodes.size(),
                                        (void*) octreeNodes.data());
  }

  void OctreeGridSearch::Impl::identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                                               const std::vector<PrimitiveID>&    regionIDs,
                                               Point                              mid) {
    u_long regionSize = regionIDs.size();

    cl::Buffer targets_buf(clContext,
                           CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                           sizeof(std::array<bool, 8>) * regionSize,
                           (void*) targets.data());
    cl::Buffer regionIDs_buf = cl::Buffer(clContext,
                                          CL_MEM_READ_ONLY | CL_MEM_HOST_NO_ACCESS | CL_MEM_COPY_HOST_PTR,
                                          sizeof(PrimitiveID) * regionSize,
                                          (void*) regionIDs.data());

    cl::Kernel kernel(clIdentifyTargetsProgram, "identifyTargetsKernel");
    kernel.setArg(0, tetras_buf);
    kernel.setArg(1, regionIDs_buf);
    kernel.setArg(2, mid);
    kernel.setArg(3, targets_buf);

    cl::CommandQueue queue(clContext, clDevice);

    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(regionSize));
    queue.enqueueReadBuffer(targets_buf, CL_TRUE, 0, sizeof(std::array<bool, 8>) * regionSize, targets.data());
  }


  void OctreeGridSearch::Impl::split(Octree&                          octree,
                                     std::vector<PrimitiveID>&        regions,
                                     std::vector<Octree>&             octreeNodes,
                                     const std::vector<PrimitiveID>&  regionIDs) {

    auto currentRegionSize = octree.regionSize;
    if(currentRegionSize <= maxRegionSize) {
      octree.regionStart = regions.size();
      regions.insert(std::end(regions), std::begin(regionIDs), std::end(regionIDs));
      return;
    }

    auto ownID = octreeNodes.size() -1;
    auto mid = octree.aaBox.mid();
    auto octants = octree.aaBox.octants();
    octree.leftChild = octreeNodes.size();//leftChild will be inserted next

    std::vector<std::array<bool, 8>> targets(currentRegionSize, {false});
    identifyTargets(targets, regionIDs, mid);

    auto lastChildID = octree.leftChild;

    for(u_short k=0; k<8; k++) {

      RegionSizeT childRegionSize = 0;
      std::vector<PrimitiveID> childRegionIDs;
      for(RegionSizeT i=0; i<currentRegionSize; i++)
        if(targets[i][k]) {
          childRegionIDs.emplace_back(regionIDs[i]);
          childRegionSize++;
        }

      Octree child;
      child.aaBox = octants[k];
      child.regionSize = childRegionSize;
      child.parent = ownID;

      auto childID = octreeNodes.size();
      octreeNodes.emplace_back(child);
      octreeNodes[lastChildID].rightSibling = childID;
      lastChildID = childID;

      split(octreeNodes[childID], regions, octreeNodes, childRegionIDs);
    }
  }

  void OctreeGridSearch::setOutputBuffer(const OpenglBuffer& openglBuffer) {
    _impl->oglBuffer = openglBuffer;
  }

  void OctreeGridSearch::render(const Camera& camera) {

    PixelRGBA* texPtr = (PixelRGBA*) glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

    auto width = _impl->oglBuffer.width();
    auto height = _impl->oglBuffer.height();
    auto numPixels = width*height;

    float xAngleOffset = camera.fov() / (width - 1);
    float yAngleOffset = camera.verticalFOV() / (height - 1);

    cl::Buffer tex_buf(_impl->clContext,
                       CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY,
                       sizeof(PixelRGBA) * numPixels,
                       (void*) texPtr);

    cl::Kernel kernel(_impl->clRenderProgram, "renderKernel");
    kernel.setArg(0, _impl->tetras_buf);
    kernel.setArg(1, _impl->data_buf);
    kernel.setArg(2, _impl->regions_buf);
    kernel.setArg(3, _impl->octreeNodes_buf);
    kernel.setArg(4, camera);
    kernel.setArg(5, xAngleOffset);
    kernel.setArg(6, yAngleOffset);
    kernel.setArg(7, tex_buf);

    cl::CommandQueue queue(_impl->clContext, _impl->clDevice);

    queue.enqueueNDRangeKernel(kernel, cl::NullRange, cl::NDRange(width, height));
    queue.enqueueReadBuffer(tex_buf, CL_TRUE, 0, sizeof(PixelRGBA) * numPixels, texPtr);

    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);

  }
}