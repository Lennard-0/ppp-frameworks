//
// Created by lennard on 11/11/20.
//

#include "OctreeGridSearch.h"

#include <algorithms/Octree.h>
#include "../../../libs/scene/include/scene/Camera.h"
#include <image/PixelRGBA.h>
#include <RAJA/RAJA.hpp>

class SplitKernel;
class RenderKernel;

namespace gridbox {

  struct OctreeGridSearch::Impl {
    size_t nTetras = 0;
    uint maxRegionSize;
    RAJA::resources::Host host;
//    RAJA::resources::Omp raja_resource;
    RAJA::resources::Host raja_resource;
    Tetrahedron* tetras_buf;
    float* data_buf;
    Octree* octreeNodes_buf;
    PrimitiveID* regions_buf;
    OpenglBuffer oglBuffer;

    using EXEC_POLICY=RAJA::omp_parallel_for_exec;
//    const int GPU_BLOCK_SIZE = 256;
//    using EXEC_POLICY = RAJA::cuda_exec_async<GPU_BLOCK_SIZE>;

    void split(Octree&                            octree,
               std::vector<PrimitiveID>&          regions,
               std::vector<Octree>&               octreeNodes,
               const std::vector<PrimitiveID>&    regionIDs);

    void identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                         const std::vector<PrimitiveID>&    regionIDs,
                         Point                              mid);
  };


  OctreeGridSearch::OctreeGridSearch()
  : _impl(std::make_unique<Impl>())
  {}

//OctreeGridSearch::OctreeGridSearch(const OctreeGridSearch& other)
//: _impl(std::make_unique<Impl>(*other._impl))
//{}
//
//OctreeGridSearch::OctreeGridSearch(OctreeGridSearch&& other) = default;
//
//OctreeGridSearch& OctreeGridSearch::operator=(const OctreeGridSearch &other)
//{
//  *_impl = *other._impl;
//  return *this;
//}
//
//OctreeGridSearch& OctreeGridSearch::operator=(OctreeGridSearch&&) = default;

  OctreeGridSearch::~OctreeGridSearch() {
    if(_impl->nTetras == 0)
      return;
    _impl->raja_resource.deallocate(_impl->tetras_buf);
    _impl->raja_resource.deallocate(_impl->data_buf);
    _impl->raja_resource.deallocate(_impl->regions_buf);
    _impl->raja_resource.deallocate(_impl->octreeNodes_buf);
  }


  void OctreeGridSearch::construct(const gridbox::Grid& grid, u_long maxRegionSize) {
    LOG(INFO) << "running with: RAJA";

    _impl->nTetras = grid.size();
    _impl->maxRegionSize = maxRegionSize;

    auto* tetras_host = _impl->host.allocate<Tetrahedron>(_impl->nTetras);
    std::memcpy(tetras_host, grid.tetrahedronsPtr(), sizeof(Tetrahedron) * _impl->nTetras);
    _impl->tetras_buf = _impl->raja_resource.allocate<Tetrahedron>(_impl->nTetras);
    _impl->raja_resource.memcpy(_impl->tetras_buf, tetras_host, sizeof(Tetrahedron) * _impl->nTetras);

    auto* data_host = _impl->host.allocate<float>(_impl->nTetras);
    std::memcpy(data_host, grid.dataPtr(), sizeof(float) * _impl->nTetras);
    _impl->data_buf = _impl->raja_resource.allocate<float>(_impl->nTetras);
    _impl->raja_resource.memcpy(_impl->data_buf, data_host, sizeof(float) * _impl->nTetras);

    std::vector<Octree> octreeNodes;

    std::vector<PrimitiveID> ids(_impl->nTetras);
    std::iota(ids.begin(), ids.end(), 0);

    Octree root;
    root.aaBox = grid.aabb();
    root.regionSize = _impl->nTetras;
    octreeNodes.emplace_back(root);

    std::vector<PrimitiveID> regions;
    regions.reserve(_impl->nTetras*3);

    _impl->split(octreeNodes[0], regions, octreeNodes, ids);

    regions.shrink_to_fit();
    auto totalLeafTetras = regions.size();
    auto* regions_host = _impl->host.allocate<PrimitiveID>(totalLeafTetras);
    std::memcpy(regions_host, regions.data(), sizeof(PrimitiveID) * totalLeafTetras);
    _impl->regions_buf = _impl->raja_resource.allocate<PrimitiveID>(totalLeafTetras);
    _impl->raja_resource.memcpy(_impl->regions_buf, regions_host, sizeof(PrimitiveID) * totalLeafTetras);

    LOG(INFO) << "sum of all region sizes: " << totalLeafTetras
              << " -> x" << (double)totalLeafTetras/_impl->nTetras << " increase\n";
    LOG(INFO) << "total tree nodes: " << octreeNodes.size();

    auto* octreeNodes_host = _impl->host.allocate<Octree>(octreeNodes.size());
    std::memcpy(octreeNodes_host, octreeNodes.data(), sizeof(Octree) * octreeNodes.size());
    _impl->octreeNodes_buf = _impl->raja_resource.allocate<Octree>(octreeNodes.size());
    _impl->raja_resource.memcpy(_impl->octreeNodes_buf, octreeNodes_host, sizeof(Octree) * octreeNodes.size());

    _impl->host.deallocate(tetras_host);
    _impl->host.deallocate(data_host);
    _impl->host.deallocate(regions_host);
    _impl->host.deallocate(octreeNodes_host);
  }

  void OctreeGridSearch::Impl::identifyTargets(std::vector<std::array<bool, 8>>&  targets,
                                               const std::vector<PrimitiveID>&    regionIDs,
                                               Point                              mid) {
    u_long regionSize = regionIDs.size();

    auto* targets_host = host.allocate<std::array<bool, 8>>(targets.size());
    std::memcpy(targets_host, targets.data(), sizeof(std::array<bool, 8>) * targets.size());
    auto targets_dvc = raja_resource.allocate<std::array<bool, 8>>(targets.size());
    raja_resource.memcpy(targets_dvc, targets_host, sizeof(std::array<bool, 8>) * targets.size());

    auto* regionIDs_host = host.allocate<PrimitiveID>(regionIDs.size());
    std::memcpy(regionIDs_host, regionIDs.data(), sizeof(PrimitiveID) * regionIDs.size());
    auto regionIDs_dvc = raja_resource.allocate<PrimitiveID>(regionIDs.size());
    raja_resource.memcpy(regionIDs_dvc, regionIDs_host, sizeof(PrimitiveID) * regionIDs.size());


    RAJA::resources::Event e = RAJA::forall<EXEC_POLICY>(raja_resource, RAJA::RangeSegment(0, regionSize),
                              [=] RAJA_DEVICE (int l) {
                                auto tetraID = regionIDs_dvc[l];
                                auto tetra = tetras_buf[tetraID];
                                for(auto const& point : tetra.points()) {
                                  u_int8_t target = 0;
                                  for(u_short k=0; k<3; k++) {
                                    u_int8_t isRight = point[k] > mid[k];
                                    target += (1u<<(2u-k)) * isRight;
                                  }
                                  targets_dvc[l][target] = true;
                                }
                              });
//    raja_resource.wait_for(&e);

    raja_resource.memcpy(targets_host, targets_dvc, sizeof(std::array<bool,8>) * targets.size());
    std::memcpy(targets.data(), targets_host, sizeof(std::array<bool,8>) * targets.size());

    host.deallocate(targets_host);
    raja_resource.deallocate(targets_dvc);
    host.deallocate(regionIDs_host);
    raja_resource.deallocate(regionIDs_dvc);
  }


  void OctreeGridSearch::Impl::split(Octree&                          octree,
                                     std::vector<PrimitiveID>&        regions,
                                     std::vector<Octree>&             octreeNodes,
                                     const std::vector<PrimitiveID>&  regionIDs) {

    auto currentRegionSize = octree.regionSize;
    if(currentRegionSize <= maxRegionSize) {
      octree.regionStart = regions.size();
      regions.insert(std::end(regions), std::begin(regionIDs), std::end(regionIDs));
      return;
    }

    auto ownID = octreeNodes.size() -1;
    auto mid = octree.aaBox.mid();
    auto octants = octree.aaBox.octants();
    octree.leftChild = octreeNodes.size();//leftChild will be inserted next

    std::vector<std::array<bool, 8>> targets(currentRegionSize, {false});
    identifyTargets(targets, regionIDs, mid);

    auto lastChildID = octree.leftChild;

    for(u_short k=0; k<8; k++) {

      RegionSizeT childRegionSize = 0;
      std::vector<PrimitiveID> childRegionIDs;
      for(RegionSizeT i=0; i<currentRegionSize; i++)
        if(targets[i][k]) {
          childRegionIDs.emplace_back(regionIDs[i]);
          childRegionSize++;
        }

      Octree child;
      child.aaBox = octants[k];
      child.regionSize = childRegionSize;
      child.parent = ownID;

      auto childID = octreeNodes.size();
      octreeNodes.emplace_back(child);
      octreeNodes[lastChildID].rightSibling = childID;
      lastChildID = childID;

      split(octreeNodes[childID], regions, octreeNodes, childRegionIDs);
    }
  }

  void OctreeGridSearch::setOutputBuffer(const OpenglBuffer& openglBuffer) {
    _impl->oglBuffer = openglBuffer;
  }

  void OctreeGridSearch::render(const Camera& camera) {

    PixelRGBA* texPtr = (PixelRGBA*) glMapBuffer(GL_PIXEL_UNPACK_BUFFER, GL_WRITE_ONLY);

    auto width = _impl->oglBuffer.width();
    auto height = _impl->oglBuffer.height();
    auto numPixels = width*height;

    float xAngleOffset = camera.fov() / (width - 1);
    float yAngleOffset = camera.verticalFOV() / (height - 1);

    auto tex_host = _impl->host.allocate<PixelRGBA>(numPixels);
    auto tex_buf = _impl->raja_resource.allocate<PixelRGBA>(numPixels);


    RAJA::resources::Event e = RAJA::forall<Impl::EXEC_POLICY>(_impl->raja_resource, RAJA::RangeSegment(0, numPixels),
                                                         [=] RAJA_DEVICE (int l) {
                                                           uint x = l % width;
                                                           uint y = l / width;
                                                           uint linearIndex = l;

                                                           float xAngle = -camera.fov() / 2;
                                                           float yAngle = -camera.verticalFOV() / 2;

                                                           xAngle += x * xAngleOffset;
                                                           float xValue = tanf( toRadian(xAngle) );
                                                           yAngle += y * yAngleOffset;
                                                           float yValue = tanf( toRadian(yAngle) );

                                                           Ray ray = {camera.position(), {xValue, yValue, -1}};//todo: nicht nur horizontale richtungen erlauben

                                                           float sumMax = 20000;
                                                           float dataSum = 0;

                                                           traverse(_impl->octreeNodes_buf, ray, dataSum, sumMax, [&](Octree node) mutable {
                                                             for(RegionSizeT i=0; i<node.regionSize; i++) {
                                                               auto tetraID = _impl->regions_buf[node.regionStart + i];
                                                               auto tetra = _impl->tetras_buf[tetraID];
                                                               if(tetra.intersects(ray))
                                                                 dataSum += _impl->data_buf[tetraID];
                                                             }
                                                           });

                                                           float intensity = dataSum / sumMax;
                                                           intensity = intensity > 1 ? 1 : intensity;

                                                           tex_buf[linearIndex] = {255, (u_char) (255 - intensity * 255), (u_char) (255 - intensity * 255), 255};
                                                         });
    _impl->raja_resource.wait_for(&e);

    _impl->raja_resource.memcpy(tex_host, tex_buf, sizeof(PixelRGBA) * numPixels);
    std::memcpy(texPtr, tex_host, sizeof(PixelRGBA) * numPixels);

    _impl->host.deallocate(tex_host);
    _impl->raja_resource.deallocate(tex_buf);

    glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
  }
}