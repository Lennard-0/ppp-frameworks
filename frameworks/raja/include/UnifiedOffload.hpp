//
// Created by lennard on 12/11/20.
//

#pragma once

#include <RAJA/RAJA.hpp>

namespace gridbox {
  template<typename T>
  class SequenceHandler {
  public:
    using value_type = T;

    SequenceHandler() = default;
    SequenceHandler(std::vector<T>& dataVector)
    : _hostDataPtr(dataVector.data())
    , _hostDataPtrConst(dataVector.data())
    , _size(dataVector.size())
    {}
    SequenceHandler(const std::vector<T>& dataVector)
    : _hostDataPtrConst(dataVector.data())
    , _size(dataVector.size())
    {}

    ~SequenceHandler() {
      if(_writeBack)
        _device->memcpy(_hostDataPtr, _deviceDataPtr, sizeof(T) * _size);
      if(_deviceDataPtr)
        _device->deallocate(_deviceDataPtr);
    }

    T* data() {
      return _hostDataPtr;
    }

    T* getAccess_read(RAJA::resources::Resource* device) {
      _device = device;
      _deviceDataPtr = device->allocate<value_type>(_size);
      device->memcpy(_deviceDataPtr, _hostDataPtrConst, sizeof(value_type) * _size);
      return _deviceDataPtr;
    }

    T* getAccess_write(RAJA::resources::Resource* device) {
      _device = device;
      _deviceDataPtr = device->allocate<value_type>(_size);
      _writeBack = true;
      return _deviceDataPtr;
    }

  private:
    T* _hostDataPtr = nullptr;
    const T* _hostDataPtrConst = nullptr;
    T* _deviceDataPtr = nullptr;
    RAJA::resources::Resource* _device;
    size_t _size;
    bool _writeBack = false;
  };

  #define DEVICE_SETUP() \
    RAJA::resources::Host deviceConcrete; \
    RAJA::resources::Resource device{deviceConcrete}; \
    using EXEC_POLICY=RAJA::omp_parallel_for_exec
  #define DEVICE_FINALIZE()

  #define BEGIN_OFFLOAD_SETUP()

  #define END_OFFLOAD_SETUP()

  #define DEVICE_ACCESS(buffer, mode) buffer.getAccess_##mode(&device)
  #define BEGIN_EXECUTE(N) RAJA::resources::Event e = RAJA::forall<EXEC_POLICY>(deviceConcrete, RAJA::RangeSegment(0, N), \
    [=] RAJA_DEVICE (int idx) {

  #define END_EXECUTE() });
}