#include(blt/SetupBLT.cmake)

find_package(RAJA REQUIRED)
#blt_print_target_properties(TARGET RAJA)

if (ENABLE_CUDA)
    set (CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} --expt-extended-lambda")
endif ()

add_library(gridsearch
                src/OctreeGridSearch.cpp)
target_include_directories(gridsearch PUBLIC include)
target_compile_options(gridsearch PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_features(gridsearch PRIVATE cxx_std_17)
target_link_libraries(gridsearch PUBLIC gridbox-lib gridbox-window)
target_link_libraries(gridsearch PUBLIC RAJA)
#target_link_libraries(gridsearch PUBLIC RAJA openmp cuda)


add_library(unified-ol
                include/UnifiedOffload.hpp
                src/DummyFile.cpp)
target_include_directories(unified-ol PUBLIC include)
target_compile_options(unified-ol PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_features(unified-ol PRIVATE cxx_std_17)
target_link_libraries(unified-ol PUBLIC RAJA)