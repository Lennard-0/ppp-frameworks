//
// Created by lennard on 10/15/20.
//

#pragma once

#include "geometry/Point.h"

namespace gridbox {

  class PlaneOMP {
  public:
    Point support;
    Vector normal;

#pragma omp declare target

    PlaneOMP() = default;
    PlaneOMP(Point  sprt, Vector  nrml)
    : support(sprt)
    , normal(nrml)
    {}
    PlaneOMP(const Point& point0CCW, const Point& point1CCW, const Point& point2CCW)
      : PlaneOMP(point0CCW, (point1CCW - point0CCW).cross(point2CCW - point0CCW))
    {}

    [[nodiscard]] bool isLeft(const Point& point) const {
      auto supportPointVec = point - support;
      return supportPointVec.dot(normal) < -EPSILON; //true -> angle between normal and dir > 90
    }
    [[nodiscard]] bool isLeftOrOn(const Point& point) const;

#pragma omp end declare target
  };
}