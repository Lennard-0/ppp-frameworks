//
// Created by lennard on 12/3/20.
//

#pragma once

#include "PlaneOMP.h"

namespace gridbox {

  class TriangleOMP {
  public:
    Point pointA, pointB, pointC;

#pragma omp declare target

//    TriangleOMP();
//    TriangleOMP(const Point& pointA, const Point& pointB, const Point& pointC);
//    [[nodiscard]] std::array<Point,3> points() const;

//    [[nodiscard]] std::array<LineSegment, 3> lineSegments() const;
    [[nodiscard]] PlaneOMP plane() const {
      return {pointA, pointB, pointC};
    }
    [[nodiscard]] bool intersectsInclusive(const Ray& ray) const {
//      auto pts = points();
      Point pts[3] = {pointA, pointB, pointC};

      Vector edge1, edge2, h, s, q;
      fp_t a,f,u,v;
      edge1 = pts[1] - pts[0];
      edge2 = pts[2] - pts[0];
      h = ray.direction().cross(edge2);
      a = edge1.dot(h);
      if (a > -EPSILON && a < EPSILON)
        return false;   //ray is parallel to the triangle
      f = 1.0/a;
      s = ray.origin() - pts[0];
      u = f * s.dot(h);
      if (u < 0.0 || u > 1.0)
        return false;
      q = s.cross(edge1);
      v = f * ray.direction().dot(q);
      if (v < 0.0 || u + v > 1.0)
        return false;
      // At this stage we can compute lambda to find out where the intersection point is on the line.
      float lambda = f * edge2.dot(q);

      if (lambda < -EPSILON)
        return false; // intersection behind origin
      return true;
    }
//    [[nodiscard]] std::experimental::optional<fp_t> intersectionLambdaInclusive(const Ray& ray) const;

#pragma omp end declare target
  };
}