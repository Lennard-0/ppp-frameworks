# for using Kokkos_ROOT variable
if(CMAKE_VERSION VERSION_GREATER_EQUAL "3.12.0")
    message(STATUS "Setting policy CMP0074 to use <Package>_ROOT variables")
    cmake_policy(SET CMP0074 NEW)
endif()

find_package(Kokkos REQUIRED)

add_library(gridsearch
        src/OctreeGridSearch.cpp)
target_include_directories(gridsearch PUBLIC include)
target_compile_options(gridsearch PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_features(gridsearch PRIVATE cxx_std_17)
target_link_libraries(gridsearch PUBLIC gridbox-lib gridbox-window Kokkos::kokkos)


add_library(unified-ol
        include/UnifiedOffload.hpp
                src/DummyFile.cpp)
target_include_directories(unified-ol PUBLIC include)
target_compile_options(unified-ol PRIVATE $<$<COMPILE_LANGUAGE:CXX>:-Werror=return-type>)
target_compile_features(unified-ol PRIVATE cxx_std_17)
target_link_libraries(unified-ol PUBLIC Kokkos::kokkos)