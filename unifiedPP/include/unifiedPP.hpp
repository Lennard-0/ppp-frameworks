//
// Created by lennard on 3/1/21.
//

#pragma once

#if CF_USE_CUDA
  #include "impl/Cuda.hpp"
#elif CF_USE_OPENMP
  #include "impl/OpenMP.hpp"
#elif CF_USE_KOKKOS
  #include "impl/Kokkos.hpp"
#elif CF_USE_RAJA
  #include "impl/Raja.hpp"
#elif CF_USE_SYCL
  #include "impl/Sycl.hpp"
#elif CF_USE_OPENMP_TARGET
  #include "impl/OpenMP-Target.hpp"
#endif