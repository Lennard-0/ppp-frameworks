//
// Created by lennard on 2/28/21.
//

#pragma once
#include <tt.h>
#include <Kokkos_Core.hpp>

//#define UPP_LAMBDA KOKKOS_LAMBDA
#define UPP_LAMBDA [=]


namespace uPP {

  using range = size_t;
  using index = size_t;

  struct DataUsage {
    bool input;
    bool output;
  };
  const DataUsage Input = {true, false};
  const DataUsage Output = {false, true};
  const DataUsage InputOutput = {true, true};

  class Cuda {
  public:
    using KokkosExecSpace = Kokkos::Cuda;
    using KokkosMemSpace = Kokkos::CudaSpace;
    inline static const std::string name = "Cuda";
  };
  class OpenMP {
  public:
    using KokkosExecSpace = Kokkos::OpenMP;
    using KokkosMemSpace = Kokkos::HostSpace;
    inline static const std::string name = "OpenMP";
  };

#if CF_DEFAULT_TARGET_GPU
  using Device = Cuda;
#else
  using Device = OpenMP;
#endif

  class Context {
  public:
    Context() {
      Kokkos::initialize();
      std::cout << "initialized Kokkos backend with execution space: " << Device::name << std::endl;
    }
    ~Context() {
      Kokkos::finalize();
    }
  };
  std::shared_ptr<Context> getContext() {
    static std::shared_ptr<Context> ctx_ptr = std::make_shared<Context>();
    return ctx_ptr;
  }

  template<typename F>
  void forall(size_t N, F f) {
    auto ctx = getContext();
    Device::KokkosExecSpace ex;
    auto t0 = std::chrono::steady_clock::now();
    Kokkos::parallel_for("forall", Kokkos::RangePolicy<Device::KokkosExecSpace>(ex,0,N), f);
    auto t1 = std::chrono::steady_clock::now();
    std::cout << "Kokkos::parallel_for took " << t1-t0 << '\n';
  }

  using ArrayHandlerID = int;
  static std::atomic <ArrayHandlerID> ah_id_counter;

  template<typename T>
  class ArrayHandler {
  public:
    using Value = T;
#if CF_DEFAULT_TARGET_GPU
    using View = Kokkos::View<T*, Device::KokkosMemSpace>;
    using ConstView = Kokkos::View<T*, Device::KokkosMemSpace>;
    using HostView = Kokkos::View<T*, Kokkos::HostSpace, Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
    using ConstHostView = Kokkos::View<const T*, Kokkos::HostSpace, Kokkos::MemoryTraits<Kokkos::Unmanaged>>;
#else
    using View = T*;
    using ConstView = const T*;
#endif

    ArrayHandler(const std::vector<T>& data, DataUsage usage=Input)
    : size_{data.size()}
    , ctx_ptr_{getContext()}
#if CF_DEFAULT_TARGET_GPU
      , data_host_const_(data.data(), size_)
      , data_dvc_{"view", size_}
#else
      , data_dvc_const_(data.data())
#endif
//    , hostMirror_(Kokkos::create_mirror_view(data_dvc_))
    {
      if (usage.output)
        throw std::runtime_error("cant use const vector as output");
    }
    ArrayHandler(std::vector<T>& data, DataUsage usage=InputOutput)
    : size_{data.size()}
    , ctx_ptr_{getContext()}
#if CF_DEFAULT_TARGET_GPU
    , data_host_const_{data.data(), size_}
    , data_host_{data.data(), size_}
    , data_dvc_{"view", size_}
#else
    , data_dvc_const_{data.data()}
    , data_dvc_{data.data()}
#endif
//    , hostMirror_(Kokkos::create_mirror_view(data_dvc_))
    {
      if(usage.output)
        enableCopyBack();
    }

    ~ArrayHandler() {
      if(copyBack_) {
        auto t0 = std::chrono::steady_clock::now();
#if CF_DEFAULT_TARGET_GPU
        Kokkos::deep_copy(data_host_, data_dvc_);
#endif
        auto t1 = std::chrono::steady_clock::now();
        std::cout << "AH " << id() << ": transfering data from deviceView into hostView took " << t1-t0 << '\n';
//        t0 = std::chrono::steady_clock::now();
//        std::memcpy(data_host_, &hostMirror_(0), sizeof(T)*size_);
//        t1 = std::chrono::steady_clock::now();
//        std::cout << "AH " << id() << ": transfering data from hostMirror into host_external took " << t1-t0 << '\n';
      }
    }

    [[nodiscard]]
    auto id() const -> ArrayHandlerID {
      return id_;
    }

    size_t size() const {
      return size_;
    }

    bool constInitialized() const {
#if CF_DEFAULT_TARGET_GPU
      return data_host_.size() == 0;
#else
      return data_dvc_ == nullptr;
#endif
    }

    ConstView readView() {
      transferToDevice();
#if CF_DEFAULT_TARGET_GPU
      return data_dvc_;
#else
      return data_dvc_const_;
#endif
    }

    View writeView() {
      return data_dvc_;
    }

    View readWriteView() {
      transferToDevice();
      return data_dvc_;
    }

  private:
    void transferToDevice() {
//      auto t0 = std::chrono::steady_clock::now();
//      std::memcpy(&hostMirror_(0), data_host_const_, sizeof(T)*size_);
//      auto t1 = std::chrono::steady_clock::now();
//      std::cout << "AH " << id() << ": transfering data from host_external into hostMirror took " << t1-t0 << '\n';
//      t0 = std::chrono::steady_clock::now();
//      Kokkos::deep_copy(data_dvc_, hostMirror_);
//      t1 = std::chrono::steady_clock::now();
//      std::cout << "AH " << id() << ": transfering data from hostMirror into device View took " << t1-t0 << '\n';
//
      auto t0 = std::chrono::steady_clock::now();
#if CF_DEFAULT_TARGET_GPU
      Kokkos::deep_copy(data_dvc_, data_host_const_);
#endif
      auto t1 = std::chrono::steady_clock::now();
      std::cout << "AH " << id() << ": transfering data from hostView into deviceView took " << t1-t0 << '\n';
    }

    void enableCopyBack() {
//      if(constInitialized())
//        throw std::runtime_error("can't transfer back from device when the array was initialized with const data");
      copyBack_ = true;
    }

  private:
    ArrayHandlerID id_ = ah_id_counter++;
    std::shared_ptr<Context> ctx_ptr_;
    size_t size_;
    bool copyBack_ = false;
//    View data_dvc_;
//    typename View::HostMirror hostMirror_;
#if CF_DEFAULT_TARGET_GPU
    View data_dvc_;
    HostView data_host_;
    ConstHostView data_host_const_;
#else
    T* data_dvc_ = nullptr;
    const T* data_dvc_const_ = nullptr;
#endif
  };

}